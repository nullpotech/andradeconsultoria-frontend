import React, { useEffect, useState } from "react";
import { Keyboard, StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import { ConfirmActionModal } from "../../components/ConfirmActionModal";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultButton from "../../components/DefaultButton";
import DefaultLabelInput from "../../components/DefaultLabelInput";
import YouTubeVideo from "../../components/YouTubeVideo";
import { useWorkout } from "../../contexts/WorkoutContext";
import theme from "../../theme";
import { ScrollView } from "react-native-gesture-handler";

export default function ExerciseDetailEdit({ route, navigation }) {
  const { exercise } = route.params;
  const { updateExerciseDetails } = useWorkout();
  const [modalConfirmVisible, setModalConfirmVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  const [form, setForm] = useState({});

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () =>
    setKeyboardVisible(true)
  );

  const keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () =>
    setKeyboardVisible(false)
  );

  const fetchData = () => {
    setForm({
      sets: exercise.details ? exercise.details.sets.toString() : "3",
      repetition: exercise.details
        ? exercise.details.repetition.toString()
        : "15",
      load: exercise.details ? exercise.details.load.toString() : "50",
      intervalSeconds: exercise.details
        ? exercise.details.intervalSeconds.toString()
        : "80",
    });
    setIsLoading(false);
  };

  const handlePressSave = () => {
    updateExerciseDetails(exercise.id, form, false);
    navigation.goBack();
  };

  const handleConfirmReplicateAction = () => {
    updateExerciseDetails(exercise.id, form, true);
    navigation.goBack();
  };

  useEffect(() => {
    keyboardDidHideListener.remove();
    keyboardDidShowListener.remove();
    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>{exercise.name}</Text>
        </ComponentTitle>
      </View>

      <View style={styles.videoAndFormDetailsContainer}>
        {!isKeyboardVisible && <YouTubeVideo linkVideo={exercise.link} />}

        {isLoading ? (
          <CustomLoadingIndicator />
        ) : (
          <ScrollView>
            <DefaultLabelInput
              label="Series"
              onChange={(value) => handleInputChange("sets", value)}
              value={form.sets}
            />
            <DefaultLabelInput
              label="Carga"
              onChange={(value) => handleInputChange("repetition", value)}
              value={form.repetition}
            />
            <DefaultLabelInput
              label="Repetições"
              onChange={(value) => handleInputChange("load", value)}
              value={form.load}
            />
            <DefaultLabelInput
              label="Intervalo"
              onChange={(value) => handleInputChange("intervalSeconds", value)}
              value={form.intervalSeconds}
            />
            <View style={styles.buttonsContainer}>
              <DefaultButton
                textButton="Replicar"
                onPress={() => setModalConfirmVisible(true)}
                animation="fadeInUp"
              />
              <ConfirmActionModal
                visible={modalConfirmVisible}
                onClose={() => setModalConfirmVisible(false)}
                onConfirm={handleConfirmReplicateAction}
                modalTitle="Replicar detalhes para todos os exercícios deste treino?"
              />
              <DefaultButton
                textButton="Salvar"
                onPress={handlePressSave}
                animation="fadeInUp"
              />
            </View>
          </ScrollView>
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
  },
  navigationContainer: {
    flexShrink: 1,
    alignItems: "center",
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  buttonsContainer: {
    justifyContent: "center",
    flexDirection: "row",
    gap: theme.spacing.large,
  },
  videoAndFormDetailsContainer: {
    flex: 4,
    gap: theme.spacing.large,
    width: "100%",
  },
});
