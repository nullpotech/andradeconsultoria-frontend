import React from "react";
import { StyleSheet, Text, TextInput } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";

export default function DefaultLabelInput({
  label,
  onChange,
  value,
  keyboardType = "default",
  secureTextEntry = false,
}) {
  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <Text style={styles.formLabel}>{label}</Text>
      <TextInput
        style={styles.formInput}
        onChangeText={onChange}
        value={value}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
  },
  formLabel: {
    color: theme.colors.text,
    alignSelf: "flex-start",
    paddingLeft: theme.spacing.xlarge,
    marginLeft: theme.spacing.xlarge,
    fontSize: theme.fontSizes.medium,
  },
  formInput: {
    color: theme.colors.text,
    width: "70%",
    borderBottomColor: theme.colors.lightText,
    borderWidth: 1,
    padding: theme.spacing.medium,
    marginBottom: theme.spacing.xlarge,
  },
});
