import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";

export default function IconButton({
  iconName,
  iconSize = theme.fontSizes.xlarge,
  iconColor = theme.colors.blackText,
  buttonBackgroundColor = theme.colors.primary,
  animation = "fadeInLeft",
  onPress,
}) {
  const styles = StyleSheet.create({
    defaultButton: {
      paddingHorizontal: theme.spacing.small,
      paddingVertical: theme.spacing.small,
      borderRadius: 100,
      backgroundColor: buttonBackgroundColor,
    },
  });

  return (
    <Animatable.View animation={animation}>
      <TouchableOpacity style={styles.defaultButton} onPress={onPress}>
        {iconName && (
          <MaterialIcons name={iconName} size={iconSize} color={iconColor} />
        )}
      </TouchableOpacity>
    </Animatable.View>
  );
}
