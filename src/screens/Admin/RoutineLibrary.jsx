import FontAwesome from "@expo/vector-icons/FontAwesome";
import { useFocusEffect } from "@react-navigation/native";
import { useCallback, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { ActionsRoutineModal } from "../../components/ActionsRoutineModal";
import ComponentTitle from "../../components/ComponentTitle";
import CreateCard from "../../components/CreateCard";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultCard from "../../components/DefaultCard";
import MinimalInfo from "../../components/MinimalInfo";
import { handleError } from "../../service/errorService";
import { getFavoritesRoutines } from "../../service/routineService";
import theme from "../../theme";
import { convertRoutineToMinimalInfo } from "../../utils/converter";

export default function RoutineLibrary({ navigation }) {
  const [routines, setRoutines] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [actionsModalVisible, setActionsModalVisible] = useState(false);
  const [actionItem, setActionItem] = useState(null);

  const fetchData = () => {
    setIsLoading(true);
    getFavoritesRoutines()
      .then((response) => setRoutines(response.data))
      .catch((error) => handleError(error, "ao buscar rotinas"))
      .finally(() => setIsLoading(false));
  };

  useFocusEffect(
    useCallback(() => {
      fetchData();
    }, [])
  );

  const handleClickCreateRoutine = () => {
    navigation.navigate("RoutineCreateEdit", { id: null });
  };

  const handleLongPressRoutineItem = (item) => {
    setActionsModalVisible(true);
    setActionItem(item);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>Biblioteca de rotinas</Text>
        </ComponentTitle>
      </View>

      <View style={styles.contentContainer}>
        {isLoading ? (
          <CustomLoadingIndicator transparency={false} />
        ) : (
          <View style={styles.routineContainer}>
            <CreateCard onPress={handleClickCreateRoutine} />
            <FlatList
              data={routines.map((routine) =>
                convertRoutineToMinimalInfo(routine)
              )}
              contentContainerStyle={styles.cardList}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <FlatListRoutineItem
                  item={item}
                  navigation={navigation}
                  onLongPress={handleLongPressRoutineItem}
                />
              )}
            />
          </View>
        )}
      </View>
      <ActionsRoutineModal
        actionItem={actionItem}
        visible={actionsModalVisible}
        navigation={navigation}
        onClose={() => setActionsModalVisible(false)}
        fetchData={fetchData}
      />
    </SafeAreaView>
  );
}

const FlatListRoutineItem = ({ item, navigation, onLongPress }) => {
  const handlePress = (item) => {
    navigation.navigate("RoutineCreateEdit", { id: item.id });
  };

  return (
    <TouchableOpacity
      onPress={() => handlePress(item)}
      onLongPress={() => onLongPress(item)}
    >
      <DefaultCard>
        <MinimalInfo data={item} text1Label="PT: ">
          <FontAwesome name="angle-right" size={32} color="white" />
        </MinimalInfo>
      </DefaultCard>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: theme.spacing.medium,
    backgroundColor: theme.colors.background,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
    gap: theme.spacing.medium,
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  contentContainer: {
    flex: 10,
    width: "100%",
  },
  routineContainer: {
    width: "100%",
    gap: theme.spacing.medium,
    marginBottom: 100,
  },
  cardList: {
    gap: theme.spacing.medium,
  },
});
