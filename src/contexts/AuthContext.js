import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { createContext, useContext, useEffect, useState } from "react";
import api from "../service/api";
import { handleError } from "../service/errorService";
import { auth, getLoggedUser, userRegister } from "../service/loginService";
import { showSuccessToast } from "../utils/toastService";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const signIn = async ({ email, password }) => {
    const responseAccessToken = await auth(email, password)
      .then((response) => response.data.accessToken)
      .catch((error) => handleError(error, "ao fazer login"));

    AsyncStorage.setItem("token", responseAccessToken);

    getLoggedUser(responseAccessToken)
      .then((response) => {
        setUser(response.data);
        setIsAuthenticated(true);
      })
      .catch((error) => handleError(error, "ao buscar usuário logado"));
  };

  const signUp = async (registrationData) => {
    return await userRegister(registrationData);
  };

  const logout = async () => {
    try {
      await AsyncStorage.removeItem("token");
      setUser({});
      setIsAuthenticated(false);
    } catch (error) {
      handleError(error, "ao sair da aplicação");
    }
  };

  useEffect(() => {
    const setupAxiosInterceptors = () => {
      api.interceptors.response.use(
        (response) => response,
        async (error) => {
          if (error.response && error.response.status === 401) {
            error.response.data.error = "Sessão expirada. Faça login novamente";
            await logout();
          }
          return Promise.reject(error);
        }
      );
    };

    if (isAuthenticated) {
      setupAxiosInterceptors();
    }
  }, [isAuthenticated]);

  return (
    <AuthContext.Provider
      value={{ user, isAuthenticated, signIn, signUp, logout, setUser }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
