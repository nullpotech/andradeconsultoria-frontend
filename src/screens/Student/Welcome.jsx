import React from "react";
import * as Animatable from "react-native-animatable";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import theme from "../../theme";
import DefaultButton from "../../components/DefaultButton";

export default function Welcome({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{ gap: theme.spacing.large }}>
        <Animatable.Text animation="fadeInLeft" style={styles.welcomeText}>
          Bem vindo!
        </Animatable.Text>
        <Animatable.Text animation="fadeInRight" style={styles.welcomeText}>
          Você será redirecionado para realizar a conclusão do seu cadastro
        </Animatable.Text>
        <View style={{ alignItems: "center" }}>
          <DefaultButton textButton="Entendido" animation="fadeInUp" onPress={() => navigation.navigate('ProfileEdit')} />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
    justifyContent: "center",
  },
  welcomeText: {
    fontSize: theme.fontSizes.xlarge,
    color: theme.colors.text,
    textAlign: "center",
  },
});
