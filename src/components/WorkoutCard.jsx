import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import ExerciseCard from './ExerciseCard';
import theme from '../theme';

const WorkoutCard = ({ workout }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <TouchableOpacity onPress={toggleExpansion} activeOpacity={0.8}>
      <View style={styles.workoutCard}>
        <Text style={styles.workoutName}>{workout.name}</Text>
        <Text style={styles.workoutInfo}>{workout.info}</Text>

        {isExpanded && (
          <FlatList
            data={workout.exercises}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item: exercise }) => <ExerciseCard exercise={exercise} />}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  workoutCard: {
    backgroundColor: theme.colors.cardBackground,
    padding: theme.spacing.medium,
    marginVertical: theme.spacing.small,
    marginHorizontal: theme.spacing.medium,
    borderRadius: theme.borderRadius.medium,
    ...theme.shadow.medium,
  },
  workoutName: {
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.bold,
    color: theme.colors.text,
  },
  workoutInfo: {
    fontSize: theme.fontSizes.small,
    color: theme.colors.lightText,
    marginBottom: theme.spacing.small,
  },
});

export default WorkoutCard;
