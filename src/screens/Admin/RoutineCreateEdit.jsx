import FontAwesome from "@expo/vector-icons/FontAwesome";
import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import ContentSwitcher from "../../components/ContentSwitcher";
import CreateCard from "../../components/CreateCard";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultCard from "../../components/DefaultCard";
import MinimalInfo from "../../components/MinimalInfo";
import RoutineForm from "../../components/RoutineForm";
import { useWorkout } from "../../contexts/WorkoutContext";
import { handleError } from "../../service/errorService";
import { getRoutineById } from "../../service/routineService";
import theme from "../../theme";
import {
  defaultDataSelectListDifficulty,
  defaultDataSelectListGoal,
} from "../../utils/constDefaultDataSelectList";
import { convertWorkoutToMinimalInfo } from "../../utils/converter";
import { ConfirmActionModal } from "../../components/ConfirmActionModal";

// TODO - Quando cadastro muitas rotinas, botão de criar rotina some
export default function RoutineCreateEdit({ navigation, route }) {
  const { id, accountId } = route.params;
  const { refresh } = useWorkout();
  const [activeTab, setActiveTab] = useState("left");
  const [isLoading, setIsLoading] = useState(false);
  const [refreshContentSwitcher, setRefreshContentSwitcher] = useState(false);
  const [confirmActionModalVisible, setConfirmActionModalVisible] =
    useState(false);
  const [routine, setRoutine] = useState({
    id: null,
    name: "",
    difficulty: defaultDataSelectListDifficulty[0].value,
    goal: defaultDataSelectListGoal[0].value,
    accountId: accountId ? accountId : null,
    workouts: [],
  });

  const fetchData = (id) => {
    setIsLoading(true);
    getRoutineById(id)
      .then((response) => setRoutine(response.data))
      .catch((error) => handleError(error, "ao buscar rotina"))
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    if (id) fetchData(id);
    else if (routine.id) fetchData(routine.id);
  }, [refresh]);

  const handleSetActiveTab = (tab) => {
    if (!routine.id) {
      setRefreshContentSwitcher(!refreshContentSwitcher);
      return setConfirmActionModalVisible(true);
    }
    setActiveTab(tab);
  };

  const handleClickCreateWorkout = () => {
    navigation.navigate("WorkoutCreateEdit", {
      workoutId: null,
      routineId: routine.id,
    });
  };

  handleSubmitRoutineForm = (newRoutine) => {
    const createdRoutine = {
      ...newRoutine,
      workouts: newRoutine.workouts || [],
    };
    setRoutine(createdRoutine);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ConfirmActionModal
        visible={confirmActionModalVisible}
        onConfirm={() => setConfirmActionModalVisible(false)}
        showCancelButton={false}
        confirmButtonText="OK"
        modalTitle="Precisa salvar a rotina antes de adicionar treinos"
      />
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>
            {routine && routine.name ? routine.name : "Nova Rotina"}
          </Text>
        </ComponentTitle>
        <ContentSwitcher
          labelLeftContent="Rotina"
          labelRightContent="Treinos"
          refresh={refreshContentSwitcher}
          onActiveButtonChange={handleSetActiveTab}
        />
      </View>

      <View style={styles.contentContainer}>
        {isLoading ? (
          <CustomLoadingIndicator transparency={false} />
        ) : activeTab === "left" ? (
          <RoutineForm
            initialValues={routine}
            onSubmit={handleSubmitRoutineForm}
          />
        ) : (
          <View style={styles.workoutsContainer}>
            <CreateCard onPress={handleClickCreateWorkout} />
            <FlatList
              data={routine.workouts.map((workout) =>
                convertWorkoutToMinimalInfo(workout)
              )}
              contentContainerStyle={styles.cardList}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <FlatListWorkoutItem item={item} navigation={navigation} />
              )}
            />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
}

const FlatListWorkoutItem = ({ item, navigation }) => {
  const handlePress = (item) => {
    navigation.navigate("WorkoutCreateEdit", { workoutId: item.id });
  };

  return (
    <TouchableOpacity onPress={() => handlePress(item)}>
      <DefaultCard>
        <MinimalInfo data={item} colorized={false}>
          <FontAwesome name="angle-right" size={32} color="white" />
        </MinimalInfo>
      </DefaultCard>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: theme.spacing.medium,
    backgroundColor: theme.colors.background,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  contentContainer: {
    flex: 5,
    width: "100%",
  },
  workoutsContainer: {
    width: "100%",
    gap: theme.spacing.medium,
    marginBottom: 100,
  },
  cardList: {
    gap: theme.spacing.medium,
  },
});
