import React from "react";
import { Image, SafeAreaView, StyleSheet } from "react-native";

export default function Logo() {
  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require("../../assets/logo-retangular.png")}
        style={styles.image}
        resizeMode="contain"
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "100%",
  },
});
