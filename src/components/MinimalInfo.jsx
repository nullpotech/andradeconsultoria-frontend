import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import theme from "../theme";
import CustomLoadingIndicator from "./CustomLoadingIndicator";
import { convertStringToDate } from "../utils/converter";

export default function MinimalInfo({
  data,
  text1Label,
  colorized = true,
  children,
}) {
  const [isLoading, setIsLoading] = useState(true);
  const [customStyleText1, setCustomStyleText1] = useState({});
  const [customStyleText3, setCustomStyleText3] = useState({});
  const defaultImage = require("../../assets/photoPerfil1.png");

  useEffect(() => {
    if (data.text1 && colorized) {
      setCustomStyleText1(defineCustomStyleColorToText1(data.text1));
      setCustomStyleText3(defineCustomStyleColorToText3(data.text3));
    }
  }, []);

  const defineCustomStyleColorToText1 = (date) => {
    const nowDate = new Date();
    const checkDate = new Date(convertStringToDate(date));

    const differenceInTime = checkDate.getTime() - nowDate.getTime();
    const differenceInDays = Math.ceil(differenceInTime / (1000 * 3600 * 24));

    let text1Color;

    // TODO ajustar para vermelho quando venceu e amarelo quando falta uma semana
    if (differenceInDays <= 30) {
      text1Color = theme.colors.error;
    } else if (differenceInDays <= 60) {
      text1Color = theme.colors.warning;
    } else {
      text1Color = theme.colors.text;
    }

    return { color: text1Color };
  };

  const defineCustomStyleColorToText3 = (text) => {
    if (text === "EXPIRADO") {
      return { color: theme.colors.error };
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        {isLoading && <CustomLoadingIndicator transparency={false} />}
        {data.photo ? (
          <Image
            source={{ uri: data.photo }}
            style={styles.image}
            onLoad={() => setIsLoading(false)}
          />
        ) : (
          <ProfilePlaceholder onLoad={setIsLoading} />
        )}
      </View>
      <View style={styles.containerInfos}>
        <Text style={styles.cardTitle} numberOfLines={1}>
          {data.title}
        </Text>
        <View>
          <Text style={[styles.cardText, customStyleText1]}>
            {text1Label} {data.text1}
          </Text>
          {data.text2 && <Text style={styles.cardText}>{data.text2}</Text>}
        </View>
        {data.text3 && (
          <Text style={[styles.textPlan, customStyleText3]}>{data.text3}</Text>
        )}
      </View>
      <View style={styles.containerButtons}>{children}</View>
    </View>
  );
}

const ProfilePlaceholder = ({ onLoad }) => {
  useEffect(() => {
    onLoad(false);
  }, []);

  return <View style={styles.profilePlaceholder} />;
};

const styles = StyleSheet.create({
  container: {
    gap: theme.spacing.medium,
    flexDirection: "row",
  },
  containerImage: {
    flex: 1.2,
    justifyContent: "center",
  },
  image: {
    borderRadius: theme.borderRadius.full,
    width: 76,
    height: 76,
  },
  containerInfos: {
    flex: 3,
    gap: theme.spacing.small,
    justifyContent: "center",
  },
  cardTitle: {
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.bold,
    color: theme.colors.text,
  },
  cardText: {
    color: theme.colors.text,
  },
  textPlan: {
    color: theme.colors.primary,
  },
  containerButtons: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: theme.spacing.medium,
  },
  profilePlaceholder: {
    borderRadius: theme.borderRadius.full,
    width: 76,
    height: 76,
    backgroundColor: "#333",
  },
});
