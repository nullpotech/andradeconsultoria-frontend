import Checkbox from "expo-checkbox";
import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { handleError } from "../service/errorService";
import { getAllExercisesNotInWorkout, getAllMinimalExercises } from "../service/exerciseService";
import { getAllMuscleGroups } from "../service/muscleGroupService";
import theme from "../theme";
import FilterSelectList from "./FilterSelectList";

export default function DefaultCheckbox({
  workoutId,
  selectedExercises,
  onSelectionChange,
}) {
  const [exercises, setExercises] = useState([]);
  const [filteredExercises, setFilteredExercises] = useState([]);
  const [muscleGroups, setMuscleGroups] = useState([]);

  const fetchExercises = async () => {
    getAllExercisesNotInWorkout(workoutId)
      .then((response) => {
        setExercises(response.data);
        setFilteredExercises(response.data);
      })
      .catch((error) => {
        handleError(error, "ao buscar exercícios");
      });
  };

  const fetchMuscleGroups = async () => {
    getAllMuscleGroups()
      .then((response) => {
        setMuscleGroups(response.data);
      })
      .catch((error) => {
        handleError(error, "ao buscar grupos musculares");
      });
  };

  useEffect(() => {
    fetchExercises();
    fetchMuscleGroups();
  }, []);

  const filterByMuscleGroup = (muscleGroup) => {
    if (muscleGroup) {
      const filtered = exercises.filter(
        (exercise) => exercise.muscleGroup.name === muscleGroup
      );
      setFilteredExercises(filtered);
    } else {
      setFilteredExercises(exercises);
    }
  };

  const toggleExerciseSelection = (exercise) => {
    let updatedSelections;
    if (selectedExercises.includes(exercise)) {
      updatedSelections = selectedExercises.filter(
        (ex) => ex.name !== exercise.name
      );
    } else {
      updatedSelections = [...selectedExercises, exercise];
    }
    onSelectionChange(updatedSelections);
  };

  const renderExercise = ({ item }) => {
    return (
      <View style={styles.exerciseItem}>
        <Checkbox
          value={selectedExercises.includes(item)}
          onValueChange={() => toggleExerciseSelection(item)}
          color={theme.colors.primary}
        />
        <Text style={styles.exerciseItemText}>{item.name}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.categoryFilter}>
        <FilterSelectList data={muscleGroups} onChange={filterByMuscleGroup} />
      </View>
      <FlatList
        data={filteredExercises}
        renderItem={renderExercise}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  categoryFilter: {
    zIndex: 1,
    flexDirection: "row",
    marginBottom: 16,
  },
  exerciseItem: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
    gap: theme.spacing.small,
  },
  exerciseItemText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.medium,
  },
});
