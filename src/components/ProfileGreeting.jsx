import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import theme from "../theme";

export default function ProfileGreeting({
  userName,
  userProfileImageUrl,
  customStyle,
  navigation,
}) {
  return (
    <View style={[style.containerHeader, customStyle]}>
      <Image source={userProfileImageUrl} style={style.roundedImage} />
      <View style={style.containerGreetingText}>
        <Text style={style.greetingMessage}>Olá, {userName}!</Text>
        <TouchableOpacity onPress={() => navigation.navigate("ProfileEdit")}>
          <Text style={style.textLink}>Editar Perfil</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  containerHeader: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: theme.spacing.medium,
  },
  containerGreetingText: {
    paddingLeft: theme.spacing.medium,
  },
  roundedImage: {
    width: 46,
    height: 46,
    borderRadius: 25,
  },
  greetingMessage: {
    color: theme.colors.text,
    fontWeight: theme.fontWeight.bold,
    fontSize: theme.fontSizes.large,
  },
  textLink: {
    color: theme.colors.lightText,
  },
});
