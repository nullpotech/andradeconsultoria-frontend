export const convertDateToString = (date) => {
  if (typeof date === "string") {
    return date;
  }
  return date.toLocaleDateString("pt-BR");
};

export const convertStringToDate = (dateString) => {
  if (typeof date === "string") {
    return dateString;
  }

  let parts = "";

  try {
    parts = dateString.split("/");
  } catch (error) {
    if (error instanceof TypeError) {
      dateString = convertDateToString(dateString);
      parts = dateString.split("/");
    } else {
      throw error;
    }
  }

  if (parts.length === 3) {
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10) - 1;
    const year = parseInt(parts[2], 10);

    return new Date(year, month, day);
  } else {
    const date = new Date(dateString);
    if (!isNaN(date)) {
      return date;
    } else {
      console.error("Formato de data inválido:", dateString);
      return null;
    }
  }
};

export const convertAccountToMinimalInfo = ({
  id,
  displayName,
  expirationDate,
  plan,
  photo,
  email,
}) => {
  let translatedPlan = '';

  if (plan === "NORMAL_STUDENT") translatedPlan = "NORMAL";
  if (plan === "SKULL_STUDENT") translatedPlan = "CAVEIRA";
  if (plan === "EXPIRED") translatedPlan = "EXPIRADO";

  return {
    id,
    title: displayName ? displayName : email,
    text1: expirationDate,
    text3: translatedPlan,
    photo,
  };
};

export const convertRoutineToMinimalInfo = ({
  id,
  name,
  goal,
  difficulty,
  endDate,
}) => {
  const endDateString = convertDateToString(endDate);
  const translatedGoal = translateGoal(goal);
  const translatedDifficulty = translateDifficulty(difficulty);

  return {
    id,
    title: name,
    text1: endDateString,
    text3: `${translatedGoal} - ${translatedDifficulty}`,
  };
};

export const convertWorkoutToMinimalInfo = ({
  id,
  name,
  info,
}) => {
  return {
    id,
    title: name,
    text1: info,
  };
};

function translateGoal(goal) {
    const translates = {
        "HYPERTROPHY": "Hipertrofia",
        "PHISYCAL_CONDITIONING": "Cond. Físico",
    };

    return translates[goal] || goal;
}

function translateDifficulty(difficulty) {
    const translates = {
        "BEGINNER": "Iniciante",
    };

    return translates[difficulty] || difficulty;
}
