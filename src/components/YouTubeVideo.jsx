import React, { useCallback, useState } from "react";
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import YouTubePlayer from "react-native-youtube-iframe";
import theme from "../theme";
import DefaultButton from "./DefaultButton";

export default function YouTubeVideo({ linkVideo }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [playing, setPlaying] = useState(false);

  const getYouTubeVideoId = (url) => {
    const regex = /[?&]v=([^&#]*)/;
    const shortRegex = /youtu\.be\/([^&#]*)/;
    let videoId = null;

    if (url.includes("youtu.be")) {
      const match = url.match(shortRegex);
      videoId = match && match[1];
    } else {
      const match = url.match(regex);
      videoId = match && match[1];
    }

    return videoId;
  };

  const videoId = linkVideo ? getYouTubeVideoId(linkVideo) : null;

  const onStateChange = useCallback((state) => {
    if (state === "ended") {
      setPlaying(false);
      setModalVisible(false);
    }
  }, []);

  return (
    <View style={styles.videoContainer}>
      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
          setPlaying(true);
        }}
      >
        <Image
          source={{
            uri: `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`,
          }}
          style={styles.thumbnail}
        />
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setPlaying(false);
        }}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <YouTubePlayer
              height={250}
              width={Dimensions.get("window").width - 40}
              play={playing}
              videoId={videoId}
              onChangeState={onStateChange}
            />
            <DefaultButton
              textButton="Fechar"
              onPress={() => {
                setModalVisible(false);
                setPlaying(false);
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  videoContainer: {
    alignItems: "center",
  },
  thumbnail: {
    width: Dimensions.get("window").width - theme.spacing.xlarge * 3,
    height:
      ((Dimensions.get("window").width - theme.spacing.xlarge * 3) * 9) / 16,
    borderRadius: theme.borderRadius.medium,
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.lightBackground,
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
  },
  modalContent: {
    backgroundColor: theme.colors.lightBackground,
    marginHorizontal: theme.spacing.medium,
    padding: theme.spacing.medium,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
  },
});
