import api from "./api";
import { getToken } from "./tokenService";

export async function createMuscleGroup(muscleGroup) {
  const bearerToken = await getToken();
  const response = await api.post(`/v1/admin/muscleGroups`, muscleGroup, {
    headers: { Authorization: bearerToken, "Content-Type": "application/json" },
  });
  return response;
}

export async function getAllMuscleGroups() {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/muscleGroups`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}
