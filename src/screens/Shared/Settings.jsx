import { StyleSheet } from "react-native";
import { Text, View } from "react-native-animatable";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import ProfileGreeting from "../../components/ProfileGreeting";
import { useAuth } from "../../contexts/AuthContext";
import theme from "../../theme";

export default function Settings({ navigation }) {
  const { user, logout } = useAuth();

  function handleLogout() {
    logout();
  }

  return (
    <SafeAreaView style={styles.container}>
      <ProfileGreeting
        userName={user?.name}
        userProfileImageUrl={require("../../../assets/photoPerfil1.png")}
        customStyle={styles.userGreeting}
        navigation={navigation}
      />
      <View style={styles.menu}>
        {user.plan === "ADMIN" && (
          <>
            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => navigation.navigate("ExerciseLibrary")}
            >
              <Text style={styles.menuItemText}>Biblioteca de Exercícios</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => navigation.navigate("RoutineLibrary")}
            >
              <Text style={styles.menuItemText}>Biblioteca de Rotinas</Text>
            </TouchableOpacity>
          </>
        )}
        <TouchableOpacity style={styles.menuLastItem} onPress={handleLogout}>
          <Text style={styles.menuLastItemText}>Sair</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: theme.colors.background,
  },
  userGreeting: {
    flex: 1,
  },
  menu: {
    flex: 3,
    width: "70%",
  },
  menuItem: {
    padding: theme.spacing.medium,
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.lightText,
  },
  menuItemText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
  },
  menuLastItem: {
    padding: theme.spacing.medium,
    alignItems: "center",
    borderBottomWidth: 1,
  },
  menuLastItemText: {
    color: theme.colors.primary,
    fontSize: theme.fontSizes.large,
  },
});
