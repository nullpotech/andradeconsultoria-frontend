import { Formik } from "formik";
import { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, Text } from "react-native";
import { View } from "react-native-animatable";
import { SafeAreaView } from "react-native-safe-area-context";
import * as Yup from "yup";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultButton from "../../components/DefaultButton";
import { DefaultDatePicker } from "../../components/DefaultDatePicker";
import DefaultSelectList from "../../components/DefaultSelectList";
import FormikLabelInput from "../../components/FormikLabelInput";
import { useAuth } from "../../contexts/AuthContext";
import { handleError } from "../../service/errorService";
import { updateLoggedUser } from "../../service/userService";
import theme from "../../theme";
import { defaultDataSelectListGender } from "../../utils/constDefaultDataSelectList";
import { convertDateToString } from "../../utils/converter";
import { showSuccessToast } from "../../utils/toastService";

export function ProfileEdit({ navigation }) {
  const { user, setUser } = useAuth();
  const [initialValues, setInitialValues] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const fetchInitialValues = () => {
    const values = {
      name: user ? user.name : "",
      lastName: user ? user.lastName : "",
      displayName: user ? user.displayName : "",
      email: user ? user.email : "",
      whatsapp: user ? user.whatsapp : "",
      birthday: user ? user.birthday : new Date(),
      gender: user ? user.gender : null,
    };
    setInitialValues(values);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchInitialValues();
  }, []);

  const handleSubmit = (user) => {
    user.birthday = convertDateToString(user.birthday);
    updateLoggedUser(user)
      .then((response) => {
        showSuccessToast("Perfil atualizado com sucesso!");
        setUser(response.data);
        if (user.plan === "ADMIN") {
          navigation.reset({
            index: 0,
            routes: [{ name: "AdminHome" }],
          });
        } else {
          navigation.reset({
            index: 0,
            routes: [{ name: "StudentHome" }],
          });
        }
      })
      .catch((error) => handleError(error, "ao atualizar perfil"));
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Campo nome é obrigatório"),
    lastName: Yup.string().required("Campo sobrenome é obrigatório"),
    displayName: Yup.string().required("Campo nome de exibição é obrigatório"),
    whatsapp: Yup.string()
      .matches(
        /^\d{13}$/,
        "Número de telefone inválido, deve usar o formato 55(código do país) + 51(DDD) + 99999000(número), sem espaços e/ou caracteres especiais"
      )
      .required("Campo whatsapp é obrigatório"),
  });

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          style={styles.roundedImage}
          source={require("../../../assets/photoPerfil1.png")}
        />
        <View style={styles.containerHeader}>
          <Text style={styles.headerText}>Editar Perfil</Text>
        </View>
      </View>

      <View style={styles.containerScroll}>
        {isLoading ? (
          <CustomLoadingIndicator />
        ) : (
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="handled"
          >
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={(values) => handleSubmit(values)}
            >
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue,
                values,
                errors,
                touched,
              }) => (
                <View style={styles.container}>
                  <FormikLabelInput
                    label="Nome"
                    value={values.name}
                    onChangeText={handleChange("name")}
                    onBlur={handleBlur("name")}
                    touched={touched.name}
                    errors={errors.name}
                  />
                  <FormikLabelInput
                    label="Sobrenome"
                    value={values.lastName}
                    onChangeText={handleChange("lastName")}
                    onBlur={handleBlur("lastName")}
                    touched={touched.lastName}
                    errors={errors.lastName}
                  />
                  <FormikLabelInput
                    label="Nome de exibição"
                    value={values.displayName}
                    onChangeText={handleChange("displayName")}
                    onBlur={handleBlur("displayName")}
                    touched={touched.displayName}
                    errors={errors.displayName}
                  />
                  <FormikLabelInput
                    label="Whatsapp"
                    value={values.whatsapp}
                    onChangeText={handleChange("whatsapp")}
                    onBlur={handleBlur("whatsapp")}
                    touched={touched.whatsapp}
                    errors={errors.whatsapp}
                    keyboardType="phone-pad"
                  />
                  <DefaultDatePicker
                    label="Data de Nascimento"
                    initialValue={values.birthday}
                    onChange={(value) => setFieldValue("birthday", value)}
                  />
                  <DefaultSelectList
                    label="Gênero"
                    data={defaultDataSelectListGender}
                    defaultOption={values.gender}
                    numberOfVisibleItems={2}
                    onChange={handleChange("gender")}
                  />
                  <View style={styles.containerButton}>
                    <DefaultButton
                      animation="fadeInUp"
                      textButton="Salvar"
                      onPress={handleSubmit}
                    />
                  </View>
                </View>
              )}
            </Formik>
          </ScrollView>
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: theme.colors.background,
  },
  containerImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    gap: theme.spacing.small,
  },
  roundedImage: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  containerHeader: {
    alignItems: "center",
    alignSelf: "center",
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.lightText,
    paddingBottom: theme.spacing.medium,
    marginBottom: theme.spacing.xlarge,
    width: "90%",
  },
  headerText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
  },
  containerScroll: {
    flex: 2,
    width: "90%",
  },
  containerButton: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: theme.spacing.xlarge,
    gap: theme.spacing.medium,
  },
  logoutText: {
    color: "red",
    fontWeight: "bold",
    fontSize: 14,
    marginTop: theme.spacing.medium,
  },
});
