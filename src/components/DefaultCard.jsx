import React from "react";
import { StyleSheet, View } from "react-native";
import theme from "../theme";

export default function DefaultCard({ children }) {
  return (
    <View style={styles.container}>{children}</View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: theme.colors.cardBackground,
    borderRadius: theme.borderRadius.large,
    padding: theme.spacing.small,
  },
});
