import { Formik } from "formik";
import React, { useRef, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from "react-native-safe-area-context";
import * as Yup from "yup";
import DefaultButton from "../../components/DefaultButton";
import FormikVerificationInput from "../../components/FormikVerificationInput";
import { handleError } from "../../service/errorService";
import {
  passwordReset,
  passwordResetVerify,
} from "../../service/passwordResetService";
import theme from "../../theme";
import { showSuccessToast } from "../../utils/toastService";
import FormikLabelInput from "../../components/FormikLabelInput";

export default function PasswordResetCode({ navigation, route }) {
  const { email } = route.params;
  const inputRefs = useRef([]);
  const [verificationCode, setVerificationCode] = useState("");
  const [isVerificationSuccess, setVerificationSuccess] = useState(false);

  const validationSchemaVerificationCode = Yup.object().shape({
    a: Yup.string().required("Campo obrigatório"),
    b: Yup.string().required("Campo obrigatório"),
    c: Yup.string().required("Campo obrigatório"),
    d: Yup.string().required("Campo obrigatório"),
    e: Yup.string().required("Campo obrigatório"),
    f: Yup.string().required("Campo obrigatório"),
  });

  const validationSchemaNewPassword = Yup.object().shape({
    password: Yup.string()
      .required("Campo senha é obrigatório")
      .min(8, "A senha deve ter pelo menos 8 caracteres")
      .test(
        "strong-password",
        "A senha deve conter pelo menos uma letra maiúscula, uma letra minúscula, um número e um caractere especial (@#$!%*?.&)",
        (value) => {
          const strongPasswordRegex =
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?.&])[A-Za-z\d@#$!%*?.&]{8,}$/;
          return strongPasswordRegex.test(value || "");
        }
      ),
    passwordConfirm: Yup.string().when("password", {
      is: () => isVerificationSuccess,
      then: (schema) =>
        schema
          .oneOf([Yup.ref("password"), null], "As senhas devem ser iguais")
          .required("Campo confirmar senha é obrigatório"),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  const resetPasswordVerify = (code) => {
    const payloadCode = {
      email,
      code: processCode(code),
    };
    console.log(payloadCode);
    passwordResetVerify(payloadCode)
      .then(() => {
        showSuccessToast("Código verificado com sucesso");
        setVerificationCode(payloadCode.code);
        setVerificationSuccess(true);
      })
      .catch((error) => handleError(error, "ao verificar código"));
  };

  const processCode = (data) => {
    const processedCode = Object.keys(data)
      .sort()
      .map((key) => data[key])
      .join("");

    return processedCode;
  };

  const handleResetPassword = (newPassword) => {
    const payload = {
      email,
      code: verificationCode,
      newPassword: newPassword.password,
    };
    console.log(payload);

    passwordReset(payload)
      .then(() => {
        showSuccessToast("Senha alterada com sucesso");
        navigation.navigate("Auth");
      })
      .catch((error) => handleError(error, "ao alterar senha"));
  };

  return (
    <SafeAreaView style={styles.container}>
      {!isVerificationSuccess ? (
        <Formik
          initialValues={{ a: "", b: "", c: "", d: "", e: "", f: "" }}
          validationSchema={validationSchemaVerificationCode}
          onSubmit={resetPasswordVerify}
        >
          {({ handleChange, handleBlur, handleSubmit, values }) => (
            <View style={styles.verificationCodeInputs}>
              <Animatable.Text
                animation="fadeInDown"
                style={styles.passwordResetTitle}
              >
                Digite o código de verificação.
              </Animatable.Text>
              <View style={styles.verificationCodeContainer}>
                {["a", "b", "c", "d", "e", "f"].map((key, index) => (
                  <FormikVerificationInput
                    key={key}
                    value={values[key]}
                    onChangeText={(text) => {
                      handleChange(key)(text);
                      if (text && index < 5) {
                        inputRefs.current[index + 1].focus();
                      }
                    }}
                    onBlur={handleBlur(key)}
                    inputRef={(ref) => (inputRefs.current[index] = ref)}
                    onKeyPress={({ nativeEvent }) => {
                      if (
                        nativeEvent.key === "Backspace" &&
                        !values[key] &&
                        index > 0
                      ) {
                        inputRefs.current[index - 1].focus();
                      }
                    }}
                  />
                ))}
              </View>
              <DefaultButton
                animation="fadeInUp"
                textButton="Verificar"
                onPress={handleSubmit}
              />
            </View>
          )}
        </Formik>
      ) : (
        <View>
          <Formik
            initialValues={{ password: "", passwordConfirm: "" }}
            validationSchema={validationSchemaNewPassword}
            onSubmit={handleResetPassword}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
            }) => (
              <View style={styles.passwordResetInputs}>
                <Animatable.Text
                  animation="fadeInRight"
                  style={styles.passwordResetTitle}
                >
                  Digite a nova senha desejada
                </Animatable.Text>
                <FormikLabelInput
                  label="Senha"
                  value={values.password}
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  touched={touched.password}
                  errors={errors.password}
                  secureTextEntry={true}
                />
                <FormikLabelInput
                  label="Repetir Senha"
                  value={values.passwordConfirm}
                  onChangeText={handleChange("passwordConfirm")}
                  onBlur={handleBlur("passwordConfirm")}
                  touched={touched.passwordConfirm}
                  errors={errors.passwordConfirm}
                  secureTextEntry={true}
                />
                <DefaultButton
                  animation="fadeInUp"
                  textButton="Alterar"
                  onPress={handleSubmit}
                />
              </View>
            )}
          </Formik>
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.colors.background,
  },
  verificationCodeInputs: {
    width: "100%",
    alignItems: "center",
    gap: theme.spacing.xlarge,
  },
  passwordResetTitle: {
    fontSize: theme.fontSizes.xlarge,
    color: theme.colors.text,
  },
  verificationCodeContainer: {
    flexDirection: "row",
    gap: theme.spacing.medium,
  },
  passwordResetInputs: {
    alignItems: "center",
    gap: theme.spacing.large,
  },
  passwordResetSubtitle: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
    marginBottom: 16,
  },
});
