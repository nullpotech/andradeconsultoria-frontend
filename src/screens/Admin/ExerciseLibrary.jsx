import { useFocusEffect } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import ContentSwitcher from "../../components/ContentSwitcher";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultSwipeList from "../../components/DefaultSwipeList";
import ExerciseForm from "../../components/ExerciseForm";
import { handleError } from "../../service/errorService";
import { deleteExercise, getAllMinimalExercises } from "../../service/exerciseService";
import theme from "../../theme";
import { showSuccessToast } from "../../utils/toastService";

export default function ExerciseLibrary({ navigation }) {
  // TODO
  /**
   * Usar o route quando cadastrar um exercicio recarregar a pagina
   * ou manter neste mesmo modelo de carregar sempre que entrar na tela
   */
  const [activeTab, setActiveTab] = useState("left");
  const [refreshSwitcher, setRefreshSwitcher] = useState(false);
  const [exercises, setExercises] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [refreshListAllExercises, setRefreshListAllExercises] = useState(false);

  useFocusEffect(
    useCallback(() => {
      setRefreshListAllExercises((prev) => !prev);
    }, [])
  );

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      getAllMinimalExercises()
        .then((response) => setExercises(response.data))
        .catch((error) => handleError(error, "ao buscar lista de exercícios"))
        .finally(() => setIsLoading(false));
    };

    fetchData();
  }, [refreshListAllExercises]);

  const handlePressEditInDefaultSwipeList = (exercise) => {
    navigation.navigate('ExerciseEdit', {id: exercise.id});
  }

  const handlePressDeleteInDefaultSwipeList = (id) => {
    deleteExercise(id)
    .then(() => {
      showSuccessToast("Exercício excluído com sucesso!");
      setRefreshListAllExercises((prev) => !prev);
    })
    .catch((error) => handleError(error, "ao buscar rotina"));
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>
            Biblioteca de exercícios
          </Text>
        </ComponentTitle>
        <ContentSwitcher
          labelLeftContent="Listar"
          labelRightContent="Cadastrar"
          onActiveButtonChange={setActiveTab}
          refresh={refreshSwitcher}
        />
      </View>

      <View style={styles.listOrCreateExerciseContainer}>
        {activeTab === "left" ? (
          isLoading ? (
            <CustomLoadingIndicator transparency={false} />
          ) : exercises.length === 0 ? (
            <View style={styles.noContentContainer}>
              <Text style={styles.noContentText}>
                Ainda não possui exercícios cadastrados
              </Text>
            </View>
          ) : (
            <DefaultSwipeList
              onDeleteRefreshStudentList={setRefreshListAllExercises}
              onConfirmDeleteAction={handlePressDeleteInDefaultSwipeList}
              onPressEdit={handlePressEditInDefaultSwipeList}
              renderData={exercises}
            />
          )
        ) : (
          <ExerciseForm
            onSubmitSetRenderComponent={setActiveTab}
            onSubmitRefreshListExercises={setRefreshListAllExercises}
            onSubmitRefreshComponentSwitcher={setRefreshSwitcher}
          />
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: theme.spacing.medium,
    backgroundColor: theme.colors.background,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  listOrCreateExerciseContainer: {
    flex: 6,
    width: "100%",
    padding: theme.spacing.medium,
  },
  noContentText: {
    color: "white",
    fontSize: theme.fontSizes.medium,
  },
  noContentContainer: {
    alignItems: "center",
  },
  exerciseItemContainer: {
    width: "80%",
    padding: theme.spacing.medium,
  },
});
