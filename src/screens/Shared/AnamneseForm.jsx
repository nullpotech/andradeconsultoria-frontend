import { SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity } from "react-native";
import { View } from "react-native-animatable";
import PropTypes from "prop-types";
import DefaultButton from "../../components/DefaultButton";
import DefaultLabelInput from "../../components/DefaultLabelInput";
import { useAuth } from "../../contexts/AuthContext";
import theme from "../../theme";
import { useState } from "react";

export function AnamneseForm({ navigation }) {
  const { user } = useAuth();
  const [form, setForm] = useState({
    weight: "",
    height: "",
    birthday: "",
    goal: "",
  });

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const handleSubmit = () => {
    console.log("Anamnese submitted:", form);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollView} keyboardShouldPersistTaps="handled">
        <View style={styles.containerHeader}>
          <Text style={styles.headerText}>Formulário de Anamnese</Text>
        </View>

        <DefaultLabelInput
          label="Peso"
          value={form.weight}
          onChangeText={(value) => handleInputChange("weight", value)}
        />
        <DefaultLabelInput
          label="Altura"
          value={form.height}
          onChangeText={(value) => handleInputChange("height", value)}
        />
        <DefaultLabelInput
          label="Data de Aniversário"
          value={form.birthday}
          onChangeText={(value) => handleInputChange("birthday", value)}
        />
        <DefaultLabelInput
          label="Objetivo"
          value={form.goal}
          onChangeText={(value) => handleInputChange("goal", value)}
        />

        <DefaultButton animation="fadeInUp" textButton="Salvar" onPress={handleSubmit} />

        <TouchableOpacity onPress={() => navigation.goBack()} activeOpacity={0.7}>
          <Text style={styles.backText}>Voltar</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
}

AnamneseForm.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: theme.colors.background,
  },
  scrollView: {
    flexGrow: 1,
    paddingHorizontal: theme.spacing.medium,
  },
  containerHeader: {
    alignItems: "center",
    alignSelf: "center",
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.lightText,
    paddingBottom: theme.spacing.medium,
    marginBottom: theme.spacing.xlarge,
    width: "90%",
  },
  headerText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
  },
  backText: {
    color: theme.colors.secondary,
    fontWeight: "bold",
    fontSize: 14,
    marginTop: theme.spacing.medium,
    textAlign: "center",
  },
});
