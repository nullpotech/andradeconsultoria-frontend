import axios from "axios";

export async function auth(email, password) {
  const credentials = btoa(`${email}:${password}`);
  const response = await axios.post(
    `http://192.168.0.3:8080/api/v1/signin`,
    {},
    {
      headers: {
        Authorization: `Basic ${credentials}`,
      },
    }
  );
  return response;
}

export async function getLoggedUser(token) {
  const response = await axios.get(`http://192.168.0.3:8080/api/v1/accounts`, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response;
}

export async function userRegister(registrationData) {
  console.log(registrationData);
  
  const response = await axios.post(
    `http://192.168.0.3:8080/api/v1/signup`,
    registrationData
  );
  return response;
}
