import React, { useState } from "react";
import {
  Dimensions,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import theme from "../theme";
import DefaultCheckbox from "./DefaultCheckbox";

const { width, height } = Dimensions.get("window");
const adjustedWidth = width - theme.spacing.xxlarge;
const adjustedHeight = height - 250;

export default function ExerciseModal({
  workoutId,
  visible,
  onClose,
  onConfirm,
}) {
  const [selectedExercises, setSelectedExercises] = useState([]);

  return (
    <Modal
      transparent={true}
      visible={visible}
      animationType="fade"
      onRequestClose={onClose}
    >
      <View style={styles.modalOverlay}>
        <View style={styles.modalContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.modalTitle}>Adicionar Exercícios</Text>
          </View>

          <View style={styles.checkboxContainer}>
            <DefaultCheckbox
              workoutId={workoutId}
              selectedExercises={selectedExercises}
              onSelectionChange={setSelectedExercises}
            />
          </View>

          <View style={styles.buttonsContainer}>
            <TouchableOpacity style={styles.cancelButton} onPress={onClose}>
              <Text style={styles.cancelButtonText}>Cancelar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.confirmButton}
              onPress={() => onConfirm(selectedExercises)}
            >
              <Text style={styles.confirmButtonText}>Adicionar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalOverlay: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Fundo semi-transparente
  },
  modalContainer: {
    width: adjustedWidth,
    height: adjustedHeight,
    padding: theme.spacing.large,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    borderColor: theme.colors.primary,
    borderWidth: theme.borders.thin,
    alignItems: "center",
    justifyContent: "center",
  },
  titleContainer: {
    height: "6%",
  },
  modalTitle: {
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
    color: theme.colors.text,
  },
  checkboxContainer: {
    height: "84%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: "10%",
  },
  cancelButton: {
    flex: 1,
    paddingVertical: theme.spacing.small,
    marginRight: theme.spacing.small,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.primary,
  },
  cancelButtonText: {
    color: theme.colors.primary,
    fontSize: theme.fontSizes.large,
  },
  confirmButton: {
    flex: 1,
    paddingVertical: theme.spacing.small,
    backgroundColor: theme.colors.primary,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
  },
  confirmButtonText: {
    color: theme.colors.blackText,
    fontSize: theme.fontSizes.large,
  },
});
