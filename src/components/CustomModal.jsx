import { useState } from "react";
import { Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import theme from "../theme";
import DefaultLabelInput from "./DefaultLabelInput";

export const CustomModal = ({ modalTitle, visible, onClose, onConfirm }) => {
  const [form, setForm] = useState("");

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const handleInputNameChange = (value) => {
    handleInputChange("name", value);
  };

  return (
    <Modal
      transparent={true}
      visible={visible}
      animationType="fade"
      onRequestClose={onClose}
    >
      <View style={styles.modalOverlay}>
        <View style={styles.modalContainer}>
          <Text style={styles.modalTitle}>Criar {modalTitle}</Text>
          <DefaultLabelInput
            onChange={handleInputNameChange}
            value={form.name}
          />
          <View style={styles.modalButtons}>
            <TouchableOpacity style={styles.cancelButton} onPress={onClose}>
              <Text style={styles.cancelButtonText}>Cancelar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.confirmButton}
              onPress={() => onConfirm(form)}
            >
              <Text style={styles.confirmButtonText}>Criar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalOverlay: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Fundo semi-transparente
  },
  modalContainer: {
    width: 300,
    padding: theme.spacing.large,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    borderColor: theme.colors.primary,
    borderWidth: theme.borders.thin,
    alignItems: "center",
  },
  modalTitle: {
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
    marginBottom: theme.spacing.small,
    color: theme.colors.text,
  },
  modalMessage: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
    marginBottom: theme.spacing.large,
    textAlign: "center",
  },
  modalButtons: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  cancelButton: {
    flex: 1,
    paddingVertical: theme.spacing.small,
    marginRight: theme.spacing.small,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.primary,
  },
  cancelButtonText: {
    color: theme.colors.primary,
    fontSize: theme.fontSizes.large,
  },
  confirmButton: {
    flex: 1,
    paddingVertical: theme.spacing.small,
    backgroundColor: theme.colors.primary,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
  },
  confirmButtonText: {
    color: theme.colors.blackText,
    fontSize: theme.fontSizes.large,
  },
});
