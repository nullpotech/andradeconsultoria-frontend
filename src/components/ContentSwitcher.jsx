import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import theme from "../theme";

export default function ContentSwitcher({
  labelLeftContent,
  labelRightContent,
  onActiveButtonChange,
  refresh,
}) {
  const [activeButton, setActiveButton] = useState("right");

  const handleButtonPress = (button) => {
    setActiveButton(button);
    onActiveButtonChange(button);
  };

  useEffect(() => {
    setActiveButton(activeButton === "left" ? "right" : "left");
  }, [refresh]);

  return (
    <View style={styles.contentSwitcherContainer}>
      <TouchableOpacity
        style={[styles.button, activeButton === "left" && styles.activeButton]}
        onPress={() => handleButtonPress("left")}
      >
        <Text
          style={
            activeButton === "left"
              ? styles.activeButtonText
              : styles.inactiveButtonText
          }
        >
          {labelLeftContent}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, activeButton === "right" && styles.activeButton]}
        onPress={() => handleButtonPress("right")}
      >
        <Text
          style={
            activeButton === "right"
              ? styles.activeButtonText
              : styles.inactiveButtonText
          }
        >
          {labelRightContent}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  contentSwitcherContainer: {
    backgroundColor: theme.colors.lightBackground,
    marginVertical: theme.spacing.small,
    borderRadius: 20,
    width: "80%",
    flexDirection: "row",
  },
  button: {
    alignItems: "center",
    padding: theme.spacing.small,
    justifyContent: "center",
    borderRadius: 20,
    width: "55%",
  },
  activeButton: {
    backgroundColor: theme.colors.primary,
  },
  activeButtonText: {
    color: theme.colors.blackText,
  },
  inactiveButtonText: {
    color: theme.colors.lightText,
  },
});
