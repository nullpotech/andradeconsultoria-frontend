import FontAwesome from "@expo/vector-icons/FontAwesome";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from "react-native-safe-area-context";
import AlternativeInput from "../../components/AlternativeInput";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultButton from "../../components/DefaultButton";
import DefaultCard from "../../components/DefaultCard";
import IconButton from "../../components/IconButton";
import Logo from "../../components/Logo";
import MinimalInfo from "../../components/MinimalInfo";
import ProfileGreeting from "../../components/ProfileGreeting";
import { useAuth } from "../../contexts/AuthContext";
import { handleError } from "../../service/errorService";
import { getAllMinimalAccounts } from "../../service/userService";
import theme from "../../theme";
import { convertAccountToMinimalInfo } from "../../utils/converter";
import { ActionsStudentModal } from "../../components/ActionsStudentModal";

const { width } = Dimensions.get("window");
const adjustedWidth = width - theme.spacing.medium;
const adjustedWidthInput = (width - theme.spacing.medium) / 2;

export default function AdminHome({ route, navigation }) {
  const { user } = useAuth();
  const { refreshListAllAccounts } = route.params || {};
  const [isFilterRender, setFilterRender] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [defaultCardData, setDefaultCardData] = useState([]);
  const [filteredCardData, setFilteredCardData] = useState([]);
  const [orderBy, setOrderBy] = useState("PT");
  const [actionsModalVisible, setActionsModalVisible] = useState(false);
  const [actionItem, setActionItem] = useState(null);

  let searchAndOrderCardData = (text, response = undefined) => {
    let orderedData = response ? response : defaultCardData;

    if (text) {
      orderedData = orderedData.filter((minimalAccount) =>
        minimalAccount.title.toLowerCase().includes(text.toLowerCase())
      );
    }

    if (orderBy === "PT") {
      orderedData = orderedData.sort((a, b) => {
        const date1 = a.text1;
        const date2 = b.text1;

        if (!(date1 && date2)) {
          return 1;
        }

        const [dayA, monthA, yearA] = date1.split("/").map(Number);
        const [dayB, monthB, yearB] = date2.split("/").map(Number);
        const dateA = new Date(yearA, monthA - 1, dayA);
        const dateB = new Date(yearB, monthB - 1, dayB);

        return dateA.getTime() - dateB.getTime();
      });
    }

    setFilteredCardData(orderedData);
  };

  useEffect(() => {
    fetchData();

    if (refreshListAllAccounts) {
      navigation.setParams({ refreshListAllAccounts: false });
    }
  }, [refreshListAllAccounts]);

  const fetchData = async () => {
    setIsLoading(true);
    getAllMinimalAccounts()
      .then((response) => {
        const responseCardData = response.data.students
          ? response.data.students.map((student) =>
              convertAccountToMinimalInfo(student)
            )
          : [];
        setDefaultCardData(responseCardData);
        searchAndOrderCardData("", responseCardData);
      })
      .catch((error) => handleError(error, "ao buscar lista de alunos"))
      .finally(() => setIsLoading(false));
  };

  const handleLongPressStudentItem = (item) => {
    setActionsModalVisible(true);
    setActionItem(item);
  };

  return (
    <SafeAreaView style={styles.container}>
      <Logo />

      <View style={styles.greetingAndFilterContainer}>
        <ProfileGreeting
          userName={user?.name}
          userProfileImageUrl={require("../../../assets/photoPerfil1.png")}
          navigation={navigation}
        />
        {isFilterRender ? (
          <IconButton
            iconName="filter-alt-off"
            iconColor={theme.colors.text}
            buttonBackgroundColor={theme.colors.cardBackground}
            onPress={() => setFilterRender(false)}
          />
        ) : (
          <IconButton
            iconName="filter-alt"
            iconColor={theme.colors.text}
            buttonBackgroundColor={theme.colors.cardBackground}
            onPress={() => setFilterRender(true)}
          />
        )}
      </View>

      {isFilterRender && (
        <Animatable.View animation="fadeInDown" style={styles.searchContainer}>
          <View style={styles.alternativeInputContainer}>
            <AlternativeInput
              onChange={(text) => searchAndOrderCardData(text, undefined)}
            />
          </View>
          {false && ( // ativar se for construir filtro no futuro
            <View style={styles.buttonsContainer}>
              <DefaultButton textButton="PA" onPress={() => setOrderBy("PA")} />
              <DefaultButton
                textButton="PT"
                backgroundColor={theme.colors.lightBackground}
                textButtonColor={theme.colors.lightText}
                onPress={() => setOrderBy("PT")}
              />
            </View>
          )}
        </Animatable.View>
      )}
      {defaultCardData.length === 0 ? (
        <View style={styles.noContentContainer}>
          <Text style={styles.noContentText}>
            Ainda não possui alunos cadastrados
          </Text>
        </View>
      ) : (
        <FlatList
          data={filteredCardData}
          contentContainerStyle={styles.cardList}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <FlatListStudentItem
              item={item}
              navigation={navigation}
              onLongPress={() => handleLongPressStudentItem(item)}
            />
          )}
        />
      )}
      {isLoading && <CustomLoadingIndicator />}
      <ActionsStudentModal
        actionItem={actionItem}
        visible={actionsModalVisible}
        navigation={navigation}
        onClose={() => setActionsModalVisible(false)}
        fetchData={fetchData}
      />
    </SafeAreaView>
  );
}

const FlatListStudentItem = ({ item, navigation, onLongPress }) => {
  const handlePress = (item) => {
    navigation.navigate("AdminStudentDetail", { id: item.id });
  };

  return (
    <TouchableOpacity
      onPress={() => handlePress(item)}
      onLongPress={() => onLongPress(item)}
    >
      <DefaultCard>
        <MinimalInfo data={item} text1Label="PT: ">
          <FontAwesome name="angle-right" size={32} color="white" />
        </MinimalInfo>
      </DefaultCard>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
    gap: theme.spacing.large,
  },
  greetingAndFilterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  searchContainer: {
    flexDirection: "row",
    gap: theme.spacing.medium,
    maxWidth: adjustedWidth,
  },
  noContentText: {
    color: "white",
    fontSize: theme.fontSizes.medium,
  },
  noContentContainer: {
    alignItems: "center",
  },
  alternativeInputContainer: {
    flexGrow: 1,
    // maxWidth: adjustedWidthInput, -- descomentar se for construir filtro
  },
  buttonsContainer: {
    flexDirection: "row",
    flexShrink: 1,
    gap: theme.spacing.small,
  },
  cardList: {
    gap: theme.spacing.medium,
  },
});
