// RoutineContext.js
import React, { createContext, useContext, useState } from "react";
import { handleError } from "../service/errorService";
import {
  createWorkout,
  getWorkoutById,
  updateWorkoutById,
} from "../service/workoutService";
import { showSuccessToast } from "../utils/toastService";

const WorkoutContext = createContext();

export const WorkoutProvider = ({ children }) => {
  const [workout, setWorkout] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);

  const loadWorkout = (workoutId, routineId) => {
    setIsLoading(true);

    if (!workoutId) {
      clearContext(routineId);
      return;
    }

    getWorkoutById(workoutId)
      .then((response) => setWorkout(response.data))
      .catch((error) => handleError(error, "ao buscar rotinas"))
      .finally(() => setIsLoading(false));
  };

  const saveWorkout = () => {
    setIsLoading(true);
    if (!workout.id) {
      createWorkout(workout)
        .then((response) => {
          showSuccessToast("Treino criado com sucesso");
          setWorkout({
            ...response.data,
            exercises: [],
          });
          setIsLoading(false);
          setRefresh(!refresh);
        })
        .catch((error) => handleError(error, "ao salvar treino"));
    } else {
      updateWorkoutById(workout)
        .then((response) => {
          showSuccessToast("Treino atualizado com sucesso");
          setIsLoading(false);
          setRefresh(!refresh);
        })
        .catch((error) => handleError(error, "ao atualizar treino"));
    }
  };

  const clearContext = (routineId) => {
    setWorkout({
      routineId: routineId ? routineId : null,
      name: "",
      info: "",
      exercises: [],
    });

    setIsLoading(false);
  };

  const addExercises = (newExercises) => {
    setWorkout((prevWorkout) => {
      const existingExerciseIds = prevWorkout.exercises.map(
        (exercise) => exercise.id
      );
      const exercisesToAdd = newExercises.filter(
        (exercise) => !existingExerciseIds.includes(exercise.id)
      );
      return {
        ...prevWorkout,
        exercises: [...prevWorkout.exercises, ...exercisesToAdd],
      };
    });
  };

  const removeExercise = (exerciseId) => {
    setWorkout((prevWorkout) => ({
      ...prevWorkout,
      exercises: prevWorkout.exercises.filter(
        (exercise) => exercise.id !== exerciseId
      ),
    }));
  };

  const updateExerciseDetails = (exerciseId, details, replicate) => {
    setWorkout((prev) => {
      const updatedExercises = prev.exercises.map((exercise) => {
        if (exercise.id === exerciseId || replicate) {
          return {
            ...exercise,
            details: { ...details },
          };
        }
        return exercise;
      });
      return {
        ...prev,
        exercises: updatedExercises,
      };
    });
  };

  return (
    <WorkoutContext.Provider
      value={{
        workout,
        isLoading,
        refresh,
        setWorkout,
        loadWorkout,
        saveWorkout,
        clearContext,
        addExercises,
        removeExercise,
        updateExerciseDetails,
      }}
    >
      {children}
    </WorkoutContext.Provider>
  );
};

export const useWorkout = () => useContext(WorkoutContext);
