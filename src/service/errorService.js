import { showErrorToast } from "../utils/toastService";

export const handleError = (error, message = "Erro inesperado!") => {
  if (error.response) {
    const status = error.response.status;
    const errorMessage = error.response.data.error;
    showErrorToast(`Erro ${message}:\n${status} - ${errorMessage}`);
  } else if (error.request) {
    showErrorToast(`Erro na requisição: ${message}. Sem resposta do servidor.`);
  } else {
    showErrorToast(`Erro inesperado: ${message}. Detalhes: ${error.message}`);
  }
};
