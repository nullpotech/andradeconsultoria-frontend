import api from "./api";
import { getToken } from "./tokenService";

export async function getFavoritesRoutines() {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/routines`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export async function getRoutineById(routineId) {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/routines/${routineId}`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export const createRoutine = async (routineData) => {
  const bearerToken = await getToken();
  const response = await api.post("/v1/admin/routines", routineData, {
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
  });
  return response;
};

export const updateRoutineById = async (routineData) => {
  const bearerToken = await getToken();
  const response = await api.put(
    `/v1/admin/routines/${routineData.id}`,
    routineData,
    {
      headers: {
        Authorization: bearerToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
};

export const deleteRoutineById = async (routineId) => {
  const bearerToken = await getToken();
  const response = await api.delete(`/v1/admin/routines/${routineId}`, {
    headers: {
      Authorization: bearerToken,
    },
  });
  return response;
};

export const cloneRoutine = async (accountId, routineId) => {
  const bearerToken = await getToken();
  const response = await api.post(
    `/v1/admin/accounts/${accountId}/routines/${routineId}`,
    {},
    {
      headers: {
        Authorization: bearerToken,

        "Content-Type": "application/json",
      },
    }
  );
  return response;
};
