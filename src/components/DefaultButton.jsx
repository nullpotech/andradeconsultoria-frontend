import AntDesign from "@expo/vector-icons/AntDesign";
import React from "react";
import { StyleSheet, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import { TouchableOpacity } from "react-native-gesture-handler";
import theme from "../theme";

export default function DefaultButton({
  textButton,
  iconName,
  animation,
  onPress,
  backgroundColor: customBackgroundColor,
  textButtonColor: customTextButtonColor,
}) {
  return (
    <Animatable.View animation={animation}>
      <TouchableOpacity
        style={[styles.defaultButton, customBackgroundColor]}
        onPress={onPress}
      >
        <Text style={[styles.textDefaultButton, customTextButtonColor]}>
          {textButton}
        </Text>
        {iconName && <AntDesign name={iconName} size={24} color="black" />}
      </TouchableOpacity>
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  defaultButton: {
    flexDirection: "row",
    alignItems: "center",
    gap: theme.spacing.medium,
    paddingHorizontal: theme.spacing.large,
    paddingVertical: theme.spacing.medium,
    borderRadius: 30,
    backgroundColor: theme.colors.primary,
  },
  textDefaultButton: {
    color: theme.colors.blackText,
    fontSize: theme.fontSizes.large,
    textTransform: "uppercase",
    fontWeight: theme.fontWeight.semiBold,
  },
});
