import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import * as Yup from "yup";
import * as Animatable from "react-native-animatable";
import theme from "../../theme";
import { Formik } from "formik";
import FormikLabelInput from "../../components/FormikLabelInput";
import DefaultButton from "../../components/DefaultButton";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import { passwordResetRequest } from "../../service/passwordResetService";
import { showSuccessToast } from "../../utils/toastService";
import { handleError } from "../../service/errorService";

export default function PasswordReset({ navigation }) {
  const [isLoading, setIsLoading] = useState(false);
  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Campo email é obrigatório"),
  });

  const resetPasswordRequest = (requestEmail) => {
    setIsLoading(true);
    passwordResetRequest(requestEmail)
      .then(() => {
        showSuccessToast("Código de verificação enviado para o e-mail");
        navigation.navigate("PasswordResetCode", { email: requestEmail.email });
      })
      .catch((error) => handleError(error, "ao enviar código de verificação"))
      .finally(() => setIsLoading(false));
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading && <CustomLoadingIndicator />}
      <Formik
        initialValues={{
          email: "robson_leal@outlook.com.br",
        }}
        validationSchema={validationSchema}
        onSubmit={resetPasswordRequest}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <View style={styles.passwordResetInputs}>
            <Animatable.Text
              animation="fadeInRight"
              style={styles.passwordResetTitle}
            >
              Digite o seu e-mail.
            </Animatable.Text>
            <FormikLabelInput
              label="Email"
              value={values.email}
              onChangeText={handleChange("email")}
              onBlur={handleBlur("email")}
              touched={touched.email}
              errors={errors.email}
              keyboardType="email-address"
            />
            <DefaultButton
              animation="fadeInUp"
              textButton="Enviar"
              onPress={handleSubmit}
            />
          </View>
        )}
      </Formik>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.colors.background,
  },
  passwordResetInputs: {
    width: "100%",
    alignItems: "center",
    gap: theme.spacing.large,
  },
  passwordResetTitle: {
    fontSize: theme.fontSizes.xlarge,
    color: theme.colors.text,
    marginBottom: 16,
  },
  passwordResetSubtitle: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
    marginBottom: 16,
  },
});
