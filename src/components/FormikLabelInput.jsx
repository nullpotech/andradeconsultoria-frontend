import { useState } from "react";
import { StyleSheet, Text, TextInput } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";

export default function FormikLabelInput({
  label,
  value,
  onChangeText,
  onBlur,
  touched,
  errors,
  keyboardType = "default",
  secureTextEntry = false,
  placeholder = "",
}) {
  const [isActive, setIsActive] = useState(false);
  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <Text
        style={[
          styles.label,
          isActive && styles.activeLabel,
          touched && errors && styles.errorLabel,
        ]}
      >
        {label}
      </Text>
      <TextInput
        style={[
          styles.input,
          isActive && styles.activeInput,
          touched && errors && styles.errorInput,
        ]}
        onChangeText={onChangeText}
        onBlur={() => {
          onBlur;
          setIsActive(false);
        }}
        onFocus={() => setIsActive(true)}
        value={value}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
        placeholder={placeholder}
      />
      {touched && errors && <Text style={styles.errorText}>{errors}</Text>}
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
  },
  label: {
    color: theme.colors.text,
    alignSelf: "flex-start",
    paddingLeft: theme.spacing.xlarge,
    marginLeft: theme.spacing.xlarge,
    fontSize: theme.fontSizes.medium,
  },
  activeLabel: {
    color: theme.colors.primary,
  },
  errorLabel: {
    color: theme.colors.error,
  },
  input: {
    color: theme.colors.text,
    width: "70%",
    borderBottomColor: theme.colors.lightText,
    borderWidth: 1,
    padding: theme.spacing.medium,
    marginBottom: theme.spacing.xlarge,
  },
  activeInput: {
    borderBottomColor: theme.colors.primary,
  },
  errorInput: {
    borderBottomColor: theme.colors.error,
    marginBottom: theme.spacing.small,
  },
  errorText: {
    color: theme.colors.error,
    marginBottom: theme.spacing.xlarge,
    textAlign: "center",
  },
});
