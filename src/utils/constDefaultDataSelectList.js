export const defaultDataSelectListGender = [
  { key: "1", value: "MALE", label: "Masculino" },
  { key: "2", value: "FEMALE", label: "Feminino" },
];

export const defaultDataSelectListGoal = [
  { key: "1", value: "PHISYCAL_CONDITIONING", label: "Condicionamento Físico" },
  { key: "2", value: "HYPERTROPHY", label: "Hipertrofia" },
];

export const defaultDataSelectListDifficulty = [
  { key: "1", value: "BEGINNER", label: "Iniciante" },
  { key: "2", value: "INTERMEDIATE", label: "Intermediário" },
  { key: "2", value: "ADVANCED", label: "Avançado" },
];

export const defaultDataSelectListPlan = [
  { key: "1", value: "NORMAL_STUDENT", label: "Normal" },
  { key: "2", value: "SKULL_STUDENT", label: "Caveira" },
];
