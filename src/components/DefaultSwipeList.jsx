import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SwipeListView } from "react-native-swipe-list-view";
import theme from "../theme";
import { ConfirmActionModal } from "./ConfirmActionModal";
import IconButton from "./IconButton";

export default function DefaultSwipeList({
  onConfirmDeleteAction,
  onPressEdit,
  renderData,
}) {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedItemId, setSelectedItemId] = useState(0);

  const handleConfirmAction = () => {
    onConfirmDeleteAction(selectedItemId);
    setModalVisible(false);
  };

  const handleEditPress = (itemId) => {
    onPressEdit(itemId);
  };

  const handleDeletePress = (itemId) => {
    setSelectedItemId(itemId);
    handleOpenModal();
  };

  const handleOpenModal = () => {
    setModalVisible(true);
  };

  const handleCloseModal = () => {
    setModalVisible(false);
  };

  const renderItem = (data) => (
    <View style={styles.rowFront}>
      <Text style={styles.rowFrontText}>{data.item.name}</Text>
    </View>
  );

  const renderHiddenItem = (data, rowMap) => (
    <View style={styles.rowBack}>
      <IconButton iconName="edit" iconSize={18} onPress={() => handleEditPress(data.item)} />
      <IconButton
        iconName="delete"
        iconSize={18}
        onPress={() => handleDeletePress(data.item.id)}
        buttonBackgroundColor={theme.colors.error}
      />
      <ConfirmActionModal
        visible={modalVisible}
        onClose={handleCloseModal}
        onConfirm={handleConfirmAction}
        modalTitle="Deseja realmente excluir o registro?"
        confirmButtonBackgroundColor={{ backgroundColor: theme.colors.error }}
        confirmButtonText="Excluir"
      />
    </View>
  );

  return (
    <SwipeListView
      data={renderData}
      renderItem={renderItem}
      renderHiddenItem={renderHiddenItem}
      rightOpenValue={-100}
    />
  );
}

const styles = StyleSheet.create({
  rowFront: {
    backgroundColor: theme.colors.background,
    padding: theme.spacing.medium,
  },
  rowFrontText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: theme.colors.background,
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    gap: theme.spacing.medium,
  },
});
