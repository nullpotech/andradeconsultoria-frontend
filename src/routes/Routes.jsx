import React from "react";
import { useAuth } from "../contexts/AuthContext";
import AdminTabs from "./AdminStack";
import AuthStack from "./AuthStack";
import StudentStack from "./StudentStack";

export default function Routes() {
  const { user, isAuthenticated } = useAuth();

  return (
    <>
      {isAuthenticated && user != null ? (
        user.plan === "ADMIN" ? (
          <AdminTabs />
        ) : (
          <StudentStack />
        )
      ) : (
        <AuthStack />
      )}
    </>
  );
}
