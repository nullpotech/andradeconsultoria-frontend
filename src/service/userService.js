import api from "./api";
import { getToken } from "./tokenService";

export async function updateLoggedUser(user) {
  const bearerToken = await getToken();
  const response = await api.put(`/v1/accounts`, user, {
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
  });
  return response;
}

export async function updateStudentPlan(id, user) {
  const bearerToken = await getToken();
  const response = await api.patch(`/v1/admin/accounts/${id}`, user, {
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
  });
  return response;
}

export async function getAllMinimalAccounts() {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/accounts`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export async function getAccountById(accountId) {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/accounts/${accountId}`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}
