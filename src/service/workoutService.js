import api from "./api";
import { getToken } from "./tokenService";

export async function getWorkoutById(workoutId) {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/workouts/${workoutId}`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export const createWorkout = async (workoutData) => {
  const bearerToken = await getToken();
  const response = await api.post(
    `/v1/admin/workouts`,
    workoutData,
    {
      headers: {
        Authorization: bearerToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
};

export const updateWorkoutById = async (workoutData) => {
  const bearerToken = await getToken();
  const response = await api.put(
    `/v1/admin/workouts/${workoutData.id}`,
    workoutData,
    {
      headers: {
        Authorization: bearerToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
};

export const updateWorkoutExerciseDetail = async (
  workoutId,
  exerciseId,
  details,
  replicate
) => {
  const bearerToken = await getToken();
  const response = await api.put(
    `/v1/admin/workouts/${workoutId}/exercises/${exerciseId}/details?replicate=${replicate}`,
    details,
    {
      headers: {
        Authorization: bearerToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
};
