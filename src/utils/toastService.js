import Toast, { BaseToast, ErrorToast } from "react-native-toast-message";
import theme from "../theme";

export const showSuccessToast = (message) => {
  Toast.show({
    type: "success",
    text1: "Sucesso",
    text2: message,
    position: "top",
    visibilityTime: 4000,
    autoHide: true,
    topOffset: 100,
  });
};

export const showErrorToast = (message) => {
  Toast.show({
    type: "error",
    text1: "Erro",
    text2: message,
    position: "top",
    visibilityTime: 4000,
    autoHide: true,
    topOffset: 100,
  });
};

export const toastConfig = {
  success: (props) => (
    <BaseToast
      {...props}
      style={{ borderLeftColor: theme.colors.primary, height: 100 }}
      contentContainerStyle={{
        backgroundColor: theme.colors.background,
      }}
      text1Style={{
        color: theme.colors.text,
        fontSize: theme.fontSizes.large,
        fontWeight: theme.fontWeight.regular,
      }}
      text2Style={{
        color: theme.colors.text,
        fontSize: theme.fontSizes.medium,
        fontWeight: theme.fontWeight.regular,
        flexWrap: "wrap",
      }}
      text2NumberOfLines={3}
    />
  ),
  error: (props) => (
    <ErrorToast
      {...props}
      style={{ borderLeftColor: theme.colors.error, height: 100 }}
      contentContainerStyle={{
        backgroundColor: theme.colors.background,
      }}
      text1Style={{
        color: theme.colors.text,
        fontSize: theme.fontSizes.large,
        fontWeight: theme.fontWeight.bold,
      }}
      text2Style={{
        color: theme.colors.text,
        fontSize: theme.fontSizes.medium,
        fontWeight: theme.fontWeight.regular,
      }}
      text2NumberOfLines={3}
    />
  ),
};
