import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultButton from "../../components/DefaultButton";
import ExerciseForm from "../../components/ExerciseForm";
import LabelInfo from "../../components/LabelInfo";
import YouTubeVideo from "../../components/YouTubeVideo";
import { handleError } from "../../service/errorService";
import { getExerciseById } from "../../service/exerciseService";
import theme from "../../theme";

export default function ExerciseEdit({ navigation, route }) {
  const { id } = route.params;
  const [isLoading, setIsLoading] = useState(true);
  const [isEditable, setIsEditable] = useState(false);
  const [exercise, setExercise] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      getExerciseById(id)
        .then((response) => setExercise(response.data))
        .catch((error) => handleError(error, "ao buscar exercício"))
        .finally(() => setIsLoading(false));
    };

    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>{exercise.name}</Text>
        </ComponentTitle>
      </View>
      {isLoading ? (
        <CustomLoadingIndicator />
      ) : (
        <View style={styles.contentContainer}>
          {isEditable ? (
            <ExerciseForm navigation={navigation} initialValues={exercise} />
          ) : (
            <View style={styles.videoAndLabelInfoContainer}>
              <YouTubeVideo linkVideo={exercise.link} />
              <LabelInfo label="Link" info={exercise.link} />
              <LabelInfo label="Grupo Muscular" info={exercise.muscleGroup} />
              <LabelInfo label="Observações" info={exercise.guideLines} />
              <View style={styles.buttonEditContainer}>
                <DefaultButton
                  textButton="Editar"
                  onPress={() => setIsEditable(true)}
                />
              </View>
            </View>
          )}
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: theme.colors.background,
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  navigationContainer: {
    flex: 1,
  },
  contentContainer: {
    flex: 8,
    width: "100%",
    padding: theme.spacing.medium,
  },
  videoAndLabelInfoContainer: {
    alignItems: "center",
    gap: theme.spacing.xlarge,
  },
  buttonEditContainer: {
    marginTop: theme.spacing.medium,
  },
});
