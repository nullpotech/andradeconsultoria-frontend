import DateTimePicker from "@react-native-community/datetimepicker";
import { useEffect, useState } from "react";
import { Platform, StyleSheet, Text, TouchableOpacity } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";
import { convertDateToString, convertStringToDate } from "../utils/converter";

export const DefaultDatePicker = ({ label, onChange, initialValue }) => {
  const [date, setDate] = useState(null);
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (initialValue) setDate(convertStringToDate(initialValue));
    else {
      setDate(new Date())
    };
  }, [initialValue]);

  const onChangeValue = (event, selectedDate) => {
    if (Platform.OS === "android") setShow(false);

    const currentDate = selectedDate;
    onChange(selectedDate);
    setDate(currentDate);
  };

  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <Text style={styles.datePickerLabel}>{label}</Text>
      <TouchableOpacity
        onPress={() => setShow(!show)}
        style={styles.datePickerInput}
      >
        {date && (
          <Text style={styles.datePickerInputText}>
            {convertDateToString(date)}
          </Text>
        )}
      </TouchableOpacity>
      {show && (
        <DateTimePicker
          value={date}
          mode="date"
          display="spinner"
          themeVariant="dark"
          onChange={onChangeValue}
        />
      )}
    </Animatable.View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
    gap: theme.spacing.small,
  },
  datePickerLabel: {
    color: theme.colors.text,
    alignSelf: "flex-start",
    paddingLeft: theme.spacing.xlarge,
    marginLeft: theme.spacing.xlarge,
    fontSize: theme.fontSizes.medium,
  },
  datePickerInput: {
    color: theme.colors.text,
    width: "70%",
    borderBottomColor: theme.colors.lightText,
    borderWidth: 1,
    padding: theme.spacing.medium,
    marginBottom: theme.spacing.xlarge,
  },
  datePickerInputText: {
    color: theme.colors.text,
  },
});
