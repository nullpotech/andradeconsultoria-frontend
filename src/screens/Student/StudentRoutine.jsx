// StudentRoutine.js

import React from 'react';
import { SafeAreaView, StyleSheet, View, FlatList } from 'react-native';
import theme from '../../theme';
import { useAuth } from '../../contexts/AuthContext';
import ProfileGreeting from '../../components/ProfileGreeting';
import Logo from '../../components/Logo';
import WorkoutCard from '../../components/WorkoutCard'; 

export default function StudentRoutine({ navigation }) {
  const { user } = useAuth();

  const workouts = user?.routines[0]?.workouts || [];

  return (
    <SafeAreaView style={styles.container}>
      <Logo />
      <View style={styles.greetingAndFilterContainer}>
        <ProfileGreeting
          userName={user?.name}
          userProfileImageUrl={require("../../../assets/photoPerfil1.png")}
          navigation={navigation}
        />
      </View>

      <FlatList
        data={workouts}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item: workout }) => (
          <WorkoutCard workout={workout} /> 
        )}
        contentContainerStyle={styles.workoutList}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
    gap: theme.spacing.large,
  },
  greetingAndFilterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  workoutList: {
    paddingBottom: 16,
  },
});
