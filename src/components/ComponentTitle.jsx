import AntDesign from "@expo/vector-icons/AntDesign";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import theme from "../theme";

export default function ComponentTitle({ navigation, children }) {
  const handlePress = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.componentTitleContainer}>
      <TouchableOpacity onPress={handlePress}>
        <AntDesign
          name="leftcircle"
          size={theme.fontSizes.xlarge}
          color={theme.colors.text}
        />
      </TouchableOpacity>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  componentTitleContainer: {
    backgroundColor: theme.colors.background,
    flexDirection: "row",
    gap: theme.spacing.medium,
    alignItems: "center",
    paddingVertical: theme.spacing.medium,
  },
});
