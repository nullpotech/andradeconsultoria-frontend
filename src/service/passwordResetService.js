import api from "./api";

export async function passwordResetRequest(email) {
  const response = await api.post(`/v1/password-reset/request`, email, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data;
}

export async function passwordResetVerify(code) {
  const response = await api.post(`/v1/password-reset/verify`, code, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data;
}

export async function passwordReset(request) {
  const response = await api.post(`/v1/password-reset/reset`, request, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.data;
}
