import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { AuthProvider } from "../contexts/AuthContext";
import Routes from "./Routes";

export default function AppNavigator() {
  return (
    <NavigationContainer>
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </NavigationContainer>
  );
}
