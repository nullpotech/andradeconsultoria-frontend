import { StatusBar } from "expo-status-bar";
import { StyleSheet, useColorScheme } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import Toast from "react-native-toast-message";
import { Colors } from "react-native/Libraries/NewAppScreen";
import "./gesture-handler";
import Routes from "./src/routes";
import { toastConfig } from "./src/utils/toastService";

export default function App() {
  const isDarkMode = useColorScheme() === "dark";

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaProvider>
      <StatusBar style="light" />
      <Routes />
      <Toast config={toastConfig} />
    </SafeAreaProvider>
  );
}
