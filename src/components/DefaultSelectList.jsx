import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import { SelectList } from "react-native-dropdown-select-list";
import theme from "../theme";

export default function DefaultSelectList({
  data,
  label,
  numberOfVisibleItems = 2,
  onChange,
  defaultOption,
}) {
  const [selected, setSelected] = useState("");
  const [selectedDefaultOption, setSelectedDefaultOption] = useState(null);
  const itemHeight = 50;

  if (
    data === null &&
    data[0].key === null &&
    data[0].value === null &&
    data[0].label === null
  ) {
    throw new Error(
      "[DefaultSelectList - ERROR] Data não pode ser nulo e deve conter key/value/label."
    );
  }

  const handleSelect = (val) => {
    const selectedOption = data.find((option) => option.label === val);
    if (selectedOption) {
      setSelected(selectedOption.value);
      onChange(selectedOption.value);
    }
  };

  useEffect(() => {
    const returnFindDefaultOption = (defaultOption) => {
      const result = data.find((option) => option.value === defaultOption);
      setSelectedDefaultOption({ key: result.key, value: result.label });
    };

    if (defaultOption) {
      returnFindDefaultOption(defaultOption);
    }
  }, []);

  const styles = StyleSheet.create({
    container: {
      alignItems: "center",
      width: "100%",
    },
    selectListLabel: {
      color: theme.colors.text,
      alignSelf: "flex-start",
      paddingLeft: theme.spacing.xlarge,
      marginLeft: theme.spacing.xlarge,
      fontSize: theme.fontSizes.medium,
    },
    inputStyles: {
      color: theme.colors.text,
    },
    boxStyles: {
      width: "70%",
      borderWidth: 0,
      borderRadius: 0,
      marginTop: theme.spacing.medium,
      borderBottomWidth: 1,
    },
    dropdownStyles: {
      borderColor: theme.colors.lightText,
      borderWidth: 1,
      maxHeight: numberOfVisibleItems * itemHeight,
    },
    dropdownTextStyles: {
      color: theme.colors.lightText,
    },
  });

  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <Text style={styles.selectListLabel}>{label}</Text>

      <SelectList
        setSelected={handleSelect}
        data={data.map((option) => ({ key: option.key, value: option.label }))}
        inputStyles={styles.inputStyles}
        placeholder="Selecionar uma opção"
        defaultOption={selectedDefaultOption}
        boxStyles={styles.boxStyles}
        dropdownStyles={styles.dropdownStyles}
        dropdownTextStyles={styles.dropdownTextStyles}
        save="value"
        search={false}
        arrowicon={
          <MaterialIcons
            name="keyboard-arrow-down"
            size={24}
            color={theme.colors.lightText}
          />
        }
      />
      <Text style={{ marginTop: 20 }}>Selected: {selected}</Text>
    </Animatable.View>
  );
}
