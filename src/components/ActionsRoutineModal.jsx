import { useState } from "react";
import { Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { handleError } from "../service/errorService";
import { deleteRoutineById } from "../service/routineService";
import theme from "../theme";
import { showSuccessToast } from "../utils/toastService";
import { ConfirmActionModal } from "./ConfirmActionModal";

export const ActionsRoutineModal = ({
  account,
  actionItem,
  visible,
  navigation,
  onClose,
  fetchData,
}) => {
  const [confirmModalVisible, setConfirmModalVisible] = useState(false);

  const handlePressEdit = () => {
    onClose();
    navigation.navigate("RoutineCreateEdit", { id: actionItem.id });
  };

  const handlePressLink = () => {
    onClose();
    navigation.navigate("RoutineLink", { accountId: account.id, routineId: actionItem.id });
  };

  const handlePressDelete = () => {
    onClose();
    setConfirmModalVisible(true);
  };

  const handleConfirmDelete = () => {
    setConfirmModalVisible(false);
    deleteRoutineById(actionItem.id)
      .then(() => {
        showSuccessToast("Rotina excluída com sucesso!");
        fetchData();
      })
      .catch((error) => handleError(error, "ao excluir rotina"));
  };

  return (
    <>
      <Modal
        transparent={true}
        visible={visible}
        animationType="fade"
        onRequestClose={onClose}
      >
        <View style={styles.modalOverlay}>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>
              {actionItem ? actionItem.title : ""}
            </Text>
            <View style={styles.modalButtons}>
              <TouchableOpacity style={styles.button} onPress={handlePressEdit}>
                <Text style={styles.buttonText}>Editar</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={handlePressLink}>
                <Text style={styles.buttonText}>Vincular</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttonDanger]}
                onPress={handlePressDelete}
              >
                <Text style={styles.buttonText}>Excluir</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttonCancel]}
                onPress={onClose}
              >
                <Text style={styles.buttonTextCancel}>Cancelar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <ConfirmActionModal
        visible={confirmModalVisible}
        modalTitle="Deseja realmente excluir a rotina?"
        onClose={() => setConfirmModalVisible(false)}
        confirmButtonText="Excluir"
        confirmButtonBackgroundColor={{ backgroundColor: theme.colors.error }}
        onConfirm={handleConfirmDelete}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalOverlay: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContainer: {
    width: 300,
    height: 350,
    padding: theme.spacing.large,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    borderColor: theme.colors.primary,
    borderWidth: theme.borders.thin,
    alignItems: "center",
    gap: theme.spacing.large,
  },
  modalTitle: {
    textAlign: "center",
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
    marginBottom: theme.spacing.small,
    color: theme.colors.text,
  },
  modalMessage: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
    marginBottom: theme.spacing.large,
    textAlign: "center",
  },
  modalButtons: {
    width: "100%",
    gap: theme.spacing.medium,
  },
  button: {
    paddingVertical: theme.spacing.small,
    marginRight: theme.spacing.small,
    backgroundColor: theme.colors.primary,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.primary,
  },
  buttonDanger: {
    backgroundColor: theme.colors.error,
    borderColor: theme.colors.danger,
  },
  buttonText: {
    color: theme.colors.blackText,
    fontSize: theme.fontSizes.large,
  },
  buttonCancel: {
    backgroundColor: theme.colors.background,
  },
  buttonTextCancel: {
    color: theme.colors.primary,
    fontSize: theme.fontSizes.large,
  },
});
