import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { handleError } from "../service/errorService";
import { createExercise, updateExerciseById } from "../service/exerciseService";
import { getAllMuscleGroups } from "../service/muscleGroupService";
import { showSuccessToast } from "../utils/toastService";
import DefaultButton from "./DefaultButton";
import DefaultLabelInput from "./DefaultLabelInput";
import DynamicSelectList from "./DynamicSelectList";

export default function ExerciseForm({
  navigation,
  onSubmitSetRenderComponent,
  onSubmitRefreshListExercises,
  onSubmitRefreshComponentSwitcher,
  initialValues,
}) {
  const [form, setForm] = useState({});

  useEffect(() => {
    setForm({
      id: initialValues ? initialValues.id : null,
      name: initialValues ? initialValues.name : "",
      link: initialValues ? initialValues.link : "",
      muscleGroup: initialValues ? initialValues.muscleGroup : "",
      guideLines: initialValues ? initialValues.guideLines : "",
    });
  }, []);

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const handleSubmitExercise = (form) => {
    if (form.id === null) {
      createExercise(form)
        .then(() => {
          onSubmitSetRenderComponent("left");
          onSubmitRefreshListExercises((prev) => !prev);
          onSubmitRefreshComponentSwitcher((prev) => !prev);
          showSuccessToast("Exercício cadastrado com sucesso");
        })
        .catch((error) => {
          handleError(error, "ao cadastrar exercício");
        });
    } else {
      updateExerciseById(form)
        .then(() => {
          showSuccessToast("Exercício atualizado com sucesso");
          navigation.navigate("ExerciseLibrary");
        })
        .catch((error) => {
          handleError(error, "ao atualizar exercício");
        });
    }
  };

  return (
    // TODO - Criar um input novo com 3 linhas para os textos maiores como obs e link
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <DefaultLabelInput
        label="Nome"
        value={form.name}
        onChange={(value) => handleInputChange("name", value)}
      />
      <DefaultLabelInput
        label="Link"
        value={form.link}
        onChange={(value) => handleInputChange("link", value)}
      />
      <DynamicSelectList
        getServiceQuery={getAllMuscleGroups}
        label="Grupo Muscular"
        onChange={(value) => handleInputChange("muscleGroup", value)}
        defaultOption={initialValues ? initialValues.muscleGroup : null}
      />
      <DefaultLabelInput
        label="Observações"
        value={form.guideLines}
        onChange={(value) => handleInputChange("guideLines", value)}
      />
      <View style={styles.buttonContainer}>
        <DefaultButton
          animation="fadeInUp"
          textButton="salvar"
          onPress={() => handleSubmitExercise(form)}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
});
