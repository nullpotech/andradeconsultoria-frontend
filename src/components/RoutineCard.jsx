import moment from "moment";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import theme from "../theme";

export default function RoutineCard({ routine, navigation }) {
  const endDate = moment(routine.endDate, "DD/MM/YYYY");
  const today = moment();

  const daysLeft = endDate.diff(today, "days");

  const dateTextStyle = daysLeft < 7 ? styles.cardTextWarning : styles.cardText;

  const handlePress = () => {
    navigation.navigate("RoutineCreateEdit", { routine });
  };

  return (
    <TouchableOpacity onPress={handlePress}>
      <View style={styles.cardContainer}>
        <View style={styles.cardTitleAndDate}>
          <Text style={styles.cardTitle}>{routine.name}</Text>
          <Text style={dateTextStyle}>Fim: {routine.endDate}</Text>
        </View>
        <View style={styles.cardContent}>
          <Text style={styles.cardText}>
            {routine.goal} - {routine.difficulty}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: theme.colors.cardBackground,
    padding: theme.spacing.medium,
    marginVertical: theme.spacing.small,
    borderRadius: theme.borderRadius.large,
  },
  cardTitleAndDate: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cardTitle: {
    fontSize: theme.fontSizes.large,
    fontWeight: "bold",
    color: theme.colors.text,
    marginBottom: theme.spacing.small,
  },
  cardContent: {
    marginTop: theme.spacing.small,
  },
  cardText: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
  },
  cardTextWarning: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.error,
  },
});
