import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";
import { SelectList } from "react-native-dropdown-select-list";

const NUMBER_OF_VISIBLE_ITEMS = 4;
const ITEM_HEIGHT = 50;

export default function FilterSelectList({ data, onChange }) {
  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <SelectList
        setSelected={onChange}
        data={data.map((option) => ({ key: option.id, value: option.name }))}
        inputStyles={styles.inputStyles}
        placeholder="Selecionar filtro"
        boxStyles={styles.boxStyles}
        dropdownStyles={styles.dropdownStyles}
        dropdownTextStyles={styles.dropdownTextStyles}
        save="value"
        search={false}
        arrowicon={
          <MaterialIcons
            name="keyboard-arrow-down"
            size={24}
            color={theme.colors.lightText}
          />
        }
      />
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
  },
  selectListLabel: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.small,
  },
  inputStyles: {
    color: theme.colors.text,
  },
  boxStyles: {
    width: "60%",
    borderWidth: 0,
  },
  dropdownStyles: {
    width: "80%",
    position: "absolute",
    top: 50,
    right: -30,
    zIndex: -10,
    backgroundColor: theme.colors.background,
    borderColor: theme.colors.primary,
    borderWidth: 1,
    maxHeight: NUMBER_OF_VISIBLE_ITEMS * ITEM_HEIGHT,
  },
  dropdownTextStyles: {
    color: theme.colors.lightText,
  },
});
