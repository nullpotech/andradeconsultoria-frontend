import React from "react";
import { StyleSheet, Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import theme from "../theme";

export default function LabelInfo({ label, info }) {
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textLabel}>{label}</Text>
      <Text style={styles.textInfo}>{info}</Text>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "70%",
  },
  textLabel: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
  },
  textInfo: {
    color: theme.colors.lightText,
    fontSize: theme.fontSizes.medium,
  },
});
