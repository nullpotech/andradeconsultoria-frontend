import { Formik } from "formik";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Animatable from "react-native-animatable";
import * as Yup from "yup";
import theme from "../theme";
import DefaultButton from "./DefaultButton";
import DefaultSwipeList from "./DefaultSwipeList";
import FormikLabelInput from "./FormikLabelInput";
import IconButton from "./IconButton";

export default function WorkoutForm({
  formData,
  onChangeInput,
  onPressAdd,
  onPressEdit,
  onPressSave,
  onPressDelete,
}) {
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Campo nome é obrigatório"),
    info: Yup.string().required("Campo descrição é obrigatório"),
  });

  return (
    <Formik
      initialValues={formData}
      validationSchema={validationSchema}
      onSubmit={(values) => onPressSave(values)}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
      }) => (
        <>
          <View style={styles.inputsContainer}>
            <FormikLabelInput
              label="Nome"
              value={values.name}
              onChangeText={(value) => {
                handleChange("name")(value);
                onChangeInput("name", value);
              }}
              onBlur={handleBlur("name")}
              touched={touched.name}
              errors={errors.name}
            />
            <FormikLabelInput
              label="Descrição"
              value={values.info}
              onChangeText={(value) => {
                handleChange("info")(value);
                onChangeInput("info", value);
              }}
              onBlur={handleBlur("info")}
              touched={touched.info}
              errors={errors.info}
            />
          </View>
          <Animatable.View
            animation="fadeInUp"
            style={styles.exercisesContainer}
          >
            <View style={styles.exercisesLabelContainer}>
              <Animatable.Text
                animation="fadeInLeft"
                style={styles.exercisesLabel}
              >
                Exercícios
              </Animatable.Text>
              <IconButton iconName="add" onPress={() => onPressAdd(true)} />
            </View>
            {formData.exercises.length === 0 ? (
              <View style={styles.noContentContainer}>
                <Text style={styles.noContentText}>
                  Ainda não possui exercícios cadastrados
                </Text>
              </View>
            ) : (
              <View style={styles.exercisesListContainer}>
                <DefaultSwipeList
                  onConfirmDeleteAction={onPressDelete}
                  onPressEdit={onPressEdit}
                  renderData={formData.exercises}
                />
              </View>
            )}
          </Animatable.View>
          <View style={styles.buttonContainer}>
            <DefaultButton
              animation="fadeInUp"
              onPress={handleSubmit}
              textButton="Salvar"
            />
          </View>
        </>
      )}
    </Formik>
  );
}

const styles = StyleSheet.create({
  inputsContainer: {
    width: "100%",
  },
  exercisesLabelContainer: {
    flexDirection: "row",
    width: "70%",
    justifyContent: "space-between",
    gap: theme.spacing.medium,
    alignItems: "center",
  },
  exercisesContainer: {
    gap: theme.spacing.medium,
  },
  exercisesListContainer: {
    height: 250,
    marginBottom: theme.spacing.medium,
  },
  exercisesLabel: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.medium,
  },
  noContentText: {
    color: "white",
    fontSize: theme.fontSizes.medium,
  },
  noContentContainer: {
    alignItems: "center",
  },
});
