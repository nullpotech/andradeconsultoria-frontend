import AsyncStorage from "@react-native-async-storage/async-storage";

export async function getToken() {
  const token = await AsyncStorage.getItem("token");
  if (!token) {
    throw new Error("Token não encontrado");
  }
  return "Bearer " + token;
}
