import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Animatable from "react-native-animatable";
import { SelectList } from "react-native-dropdown-select-list";
import { createMuscleGroup } from "../service/muscleGroupService";
import theme from "../theme";
import { showSuccessToast } from "../utils/toastService";
import { CustomModal } from "./CustomModal";
import IconButton from "./IconButton";

let numberOfVisibleItems = 2;
const itemHeight = 50;

export default function DynamicSelectList({
  getServiceQuery,
  label,
  onChange,
  numberOfVisibleItems,
  defaultOption,
}) {
  const [data, setData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedDefaultOption, setSelectedDefaultOption] = useState(null);
  const [refreshMuscleGroupList, setRefreshMuscleGroupList] = useState(false);

  const handleOpenModal = () => {
    setModalVisible(true);
  };

  const handleCloseModal = () => {
    setModalVisible(false);
  };

  const handleConfirmAction = (data) => {
    createMuscleGroup(data)
      .then(() => {
        showSuccessToast("Grupo muscular criado com sucesso");
        setRefreshMuscleGroupList((prev) => !prev);
      })
      .catch((error) => handleError(error, "ao criar grupo muscular"))
      .finally(() => {
        setModalVisible(false);
      });
  };

  const returnFindDefaultOption = (data, defaultOption) => {
    const result = data.find((option) => option.name === defaultOption);
    setSelectedDefaultOption({ key: result.name, value: result.name });
  };

  useEffect(() => {
    const fetchData = async () => {
      getServiceQuery()
        .then((response) => {
          setData(response.data);
          if (defaultOption) {
            returnFindDefaultOption(response.data, defaultOption);
          }
        })
        .catch((error) => handleError(error, "ao buscar grupos musculares"));
    };

    fetchData();
  }, [refreshMuscleGroupList]);

  numberOfVisibleItems = numberOfVisibleItems;

  return (
    <Animatable.View animation="fadeInLeft" style={styles.container}>
      <Text style={styles.selectListLabel}>{label}</Text>

      <View style={styles.selectListContent}>
        <View style={styles.inputSelectListContainer}>
          <SelectList
            setSelected={onChange}
            data={data.map((option) => ({
              key: option.id,
              value: option.name,
            }))}
            defaultOption={selectedDefaultOption}
            inputStyles={styles.inputStyles}
            placeholder="Selecionar opção"
            boxStyles={styles.boxStyles}
            dropdownStyles={styles.dropdownStyles}
            dropdownTextStyles={styles.dropdownTextStyles}
            save="value"
            search={false}
            arrowicon={
              <MaterialIcons
                name="keyboard-arrow-down"
                size={theme.fontSizes.xlarge}
                color={theme.colors.lightText}
              />
            }
          />
        </View>
        <View style={styles.buttonContainer}>
          <IconButton iconName="add" onPress={handleOpenModal} />
          <CustomModal
            visible={modalVisible}
            onClose={handleCloseModal}
            onConfirm={handleConfirmAction}
            modalTitle="Grupo Muscular"
          />
        </View>
      </View>
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
    marginBottom: theme.spacing.xlarge,
  },
  selectListLabel: {
    color: theme.colors.text,
    alignSelf: "flex-start",
    paddingLeft: theme.spacing.xlarge,
    marginLeft: theme.spacing.xlarge,
    fontSize: theme.fontSizes.medium,
  },
  selectListContent: {
    flexDirection: "row",
    width: "70%",
    gap: theme.spacing.small,
  },
  inputStyles: {
    color: theme.colors.text,
  },
  boxStyles: {
    borderWidth: 0,
    borderRadius: 0,
    borderBottomWidth: 1,
  },
  dropdownStyles: {
    borderColor: theme.colors.lightText,
    borderWidth: 1,
    maxHeight: numberOfVisibleItems * itemHeight,
  },
  inputSelectListContainer: {
    flexGrow: 1,
    marginTop: theme.spacing.medium,
  },
  buttonContainer: {
    flexShrink: 1,
    marginTop: theme.spacing.medium,
  },
  dropdownTextStyles: {
    color: theme.colors.lightText,
  },
});
