import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import theme from "../../theme";
import { getAllMinimalAccounts } from "../../service/userService";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import { ConfirmActionModal } from "../../components/ConfirmActionModal";
import { cloneRoutine } from "../../service/routineService";
import { handleError } from "../../service/errorService";
import { showSuccessToast } from "../../utils/toastService";

export default function RoutineLink({ navigation, route }) {
  const { accountId, routineId } = route.params;
  const [accounts, setAccounts] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [confirmModalVisible, setConfirmModalVisible] = useState(false);
  const [modalConfirmMessage, setModalConfirmMessage] = useState("");
  const [actionItem, setActionItem] = useState(null);

  const fetchData = () => {
    setIsLoading(true);
    getAllMinimalAccounts()
      .then((response) => {
        const filteredStudents = response.data.students.filter(
          (student) => student.id !== accountId
        );
        setAccounts(filteredStudents);
      })
      .catch((error) => handleError(error, "ao buscar lista de alunos"))
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleClickItem = (account) => {
    setModalConfirmMessage(
      `Deseja confirmar a vinculação da rotina${ account.displayName? " com " + account.displayName : ""}?`
    );
    setConfirmModalVisible(true);
    setActionItem(account);
  };

  const handleConfirmLink = () => {
    cloneRoutine(actionItem.id, routineId)
      .then(() => {
        showSuccessToast("Rotina vinculada com sucesso");
        navigation.goBack();
      })
      .catch((error) => handleError(error, "erro ao vincular rotina"))
      .finally(() => setConfirmModalVisible(false));
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <CustomLoadingIndicator transparency={false} />
      ) : (
        <>
          <View style={styles.navigationContainer}>
            <ComponentTitle navigation={navigation}>
              <Text style={styles.componentTitleText}>Vincular Rotina</Text>
            </ComponentTitle>
          </View>

          <View style={styles.contentContainer}>
            <>
              {accounts.length === 0 ? (
                <Text style={styles.noContentText}>
                  Ainda não possui alunos para vincular
                </Text>
              ) : (
                <>
                  <Text style={styles.linkRoutineText}>
                    Selecionar item para vincular...
                  </Text>
                  <TouchableOpacity onPress={handleClickItem}>
                    <Text style={styles.linkRoutineText}>
                      Biblioteca de rotinas
                    </Text>
                  </TouchableOpacity>
                  <FlatList
                    data={accounts}
                    contentContainerStyle={styles.cardList}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                      <FlatListLinkRoutineItem
                        item={item}
                        navigation={navigation}
                        onClickItem={handleClickItem}
                      />
                    )}
                  />
                </>
              )}
            </>
          </View>
          <ConfirmActionModal
            visible={confirmModalVisible}
            modalTitle={modalConfirmMessage}
            onClose={() => setConfirmModalVisible(false)}
            onConfirm={handleConfirmLink}
          />
        </>
      )}
    </SafeAreaView>
  );
}

const FlatListLinkRoutineItem = ({ item, onClickItem }) => {
  const handlePress = (item) => {
    onClickItem(item);
  };

  return (
    <TouchableOpacity onPress={() => handlePress(item)}>
      <Text style={styles.textLinkedRoutineList}>{item.displayName}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: theme.spacing.medium,
    backgroundColor: theme.colors.background,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
  },
  cardList: {
    gap: theme.spacing.medium,
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  contentContainer: {
    flex: 5,
    width: "80%",
    alignSelf: "center",
  },
  noContentText: {
    color: "white",
    fontSize: theme.fontSizes.medium,
  },
  linkRoutineText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
    marginBottom: theme.spacing.medium,
  },
  textLinkedRoutineList: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
  },
});
