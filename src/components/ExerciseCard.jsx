import React, { useState, useCallback } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, Modal } from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';
import theme from '../theme';

const ExerciseCard = ({ exercise }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [playing, setPlaying] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  const onStateChange = useCallback((state) => {
    if (state === 'ended') {
      setPlaying(false);
      setModalVisible(false);
    }
  }, []);

  const getYouTubeVideoId = (url) => {
    const regex = /[?&]v=([^&#]*)/;
    const shortRegex = /youtu\.be\/([^&#]*)/;
    let videoId = null;

    if (url.includes('youtu.be')) {
      const match = url.match(shortRegex);
      videoId = match && match[1];
    } else {
      const match = url.match(regex);
      videoId = match && match[1];
    }

    return videoId;
  };

  const videoId = exercise.link ? getYouTubeVideoId(exercise.link) : null;

  return (
    <TouchableOpacity onPress={toggleExpansion} activeOpacity={0.8}>
      <View style={styles.exerciseCard}>
        <Text style={styles.exerciseName}>{exercise.name}</Text>
        <Text style={styles.exerciseDetails}>
          Sets: {exercise.details.sets}, Reps: {exercise.details.repetition}, Load:{' '}
          {exercise.details.load}kg, Rest: {exercise.details.intervalSeconds}s
        </Text>

        {isExpanded && videoId && (
          <View style={styles.videoContainer}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(true);
                setPlaying(true);
              }}
            >
              <Image
                source={{ uri: `https://img.youtube.com/vi/${videoId}/hqdefault.jpg` }}
                style={styles.thumbnail}
              />
            </TouchableOpacity>
          </View>
        )}

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
            setPlaying(false);
          }}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <YoutubePlayer
                height={250}
                width={Dimensions.get('window').width - 40}
                play={playing}
                videoId={videoId}
                onChangeState={onStateChange}
              />
              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {
                  setModalVisible(false);
                  setPlaying(false);
                }}
              >
                <Text style={styles.closeButtonText}>Fechar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  exerciseCard: {
    backgroundColor: theme.colors.cardBackground,
    padding: theme.spacing.medium,
    marginTop: theme.spacing.small,
    borderRadius: theme.borderRadius.medium,
    ...theme.shadow.medium,
  },
  exerciseName: {
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
    color: theme.colors.text,
  },
  exerciseDetails: {
    fontSize: theme.fontSizes.small,
    color: theme.colors.lightText,
    marginTop: theme.spacing.small,
    lineHeight: theme.fontSizes.medium,
  },
  videoContainer: {
    marginTop: theme.spacing.medium,
    alignItems: 'center',
  },
  thumbnail: {
    width: Dimensions.get('window').width - theme.spacing.xlarge * 3,
    height: ((Dimensions.get('window').width - theme.spacing.xlarge * 3) * 9) / 16,
    borderRadius: theme.borderRadius.medium,
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.lightBackground,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  modalContent: {
    backgroundColor: theme.colors.lightBackground,
    marginHorizontal: theme.spacing.medium,
    padding: theme.spacing.medium,
    borderRadius: theme.borderRadius.medium,
    alignItems: 'center',
  },
  closeButton: {
    marginTop: theme.spacing.large,
    paddingVertical: theme.spacing.small,
    paddingHorizontal: theme.spacing.large,
    backgroundColor: theme.colors.error,
    borderRadius: theme.borderRadius.medium,
  },
  closeButtonText: {
    color: theme.colors.text,
    fontWeight: theme.fontWeight.semiBold,
  },
});

export default ExerciseCard;
