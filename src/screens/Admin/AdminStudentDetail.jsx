import FontAwesome from "@expo/vector-icons/FontAwesome";
import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { ActionsRoutineModal } from "../../components/ActionsRoutineModal";
import ComponentTitle from "../../components/ComponentTitle";
import { ConfirmActionModal } from "../../components/ConfirmActionModal";
import CreateCard from "../../components/CreateCard";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import DefaultButton from "../../components/DefaultButton";
import DefaultCard from "../../components/DefaultCard";
import { DefaultDatePicker } from "../../components/DefaultDatePicker";
import DefaultSelectList from "../../components/DefaultSelectList";
import MinimalInfo from "../../components/MinimalInfo";
import WhatsAppButton from "../../components/WhatsAppButton";
import { handleError } from "../../service/errorService";
import {
  cloneRoutine,
  getFavoritesRoutines,
} from "../../service/routineService";
import { getAccountById, updateStudentPlan } from "../../service/userService";
import theme from "../../theme";
import { defaultDataSelectListPlan } from "../../utils/constDefaultDataSelectList";
import {
  convertAccountToMinimalInfo,
  convertDateToString,
  convertRoutineToMinimalInfo,
} from "../../utils/converter";
import { showSuccessToast } from "../../utils/toastService";

export default function AdminStudentDetail({ route, navigation }) {
  const { id, paramActiveContent } = route.params;
  const [account, setAccount] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [activeContent, setActiveContent] = useState("routine");

  useEffect(() => {
    if (paramActiveContent) setActiveContent(paramActiveContent);
    fetchData();
  }, []);

  const fetchData = () => {
    getAccountById(id)
      .then((response) => setAccount(response.data))
      .catch((error) => handleError(error, "ao buscar estudante"))
      .finally(() => setIsLoading(false));
  };

  const handleLinkedRoutine = () => {
    setActiveContent("routine");
    fetchData();
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <CustomLoadingIndicator />
      ) : (
        <>
          <View style={styles.navigationContainer}>
            <ComponentTitle navigation={navigation}>
              <View style={styles.minimalInfoContainer}>
                <MinimalInfo
                  data={convertAccountToMinimalInfo(account)}
                  text1Label="PT: "
                >
                  <WhatsAppButton phoneNumber={account.whatsapp} />
                </MinimalInfo>
              </View>
            </ComponentTitle>
          </View>
          <View style={styles.contentContainer}>
            <View>
              {activeContent === "plan" ? (
                <PlanContent account={account} navigation={navigation} />
              ) : activeContent === "linked" ? (
                <LinkRoutineContent
                  account={account}
                  navigation={navigation}
                  reloadContent={handleLinkedRoutine}
                />
              ) : (
                <RoutineContent
                  account={account}
                  navigation={navigation}
                  fetchData={fetchData}
                />
              )}
            </View>
          </View>
        </>
      )}
    </SafeAreaView>
  );
}

const LinkRoutineContent = ({ account, navigation, reloadContent }) => {
  const [routines, setRoutines] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [modalConfirmMessage, setModalConfirmMessage] = useState("");
  const [activeRoutine, setActiveRoutine] = useState(null);
  const [visibleConfirmModal, setVisibleConfirmModal] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    setIsLoading(true);
    getFavoritesRoutines()
      .then((response) => setRoutines(response.data))
      .catch((error) => handleError(error, "ao buscar rotinas"))
      .finally(() => setIsLoading(false));
  };

  const handleClickItem = (routine) => {
    setModalConfirmMessage(
      `Deseja vincular a rotina ${routine.name} ao aluno ${account.displayName}?`
    );
    setVisibleConfirmModal(true);
    setActiveRoutine(routine);
  };

  const handleConfirmLink = () => {
    cloneRoutine(account.id, activeRoutine.id)
      .then(() => {
        showSuccessToast("Rotina vinculada com sucesso");
        reloadContent();
      })
      .catch((error) => handleError(error, "erro ao vincular rotina"))
      .finally(() => setVisibleConfirmModal(false));
  };

  return (
    <>
      <View style={styles.linkRoutineListContainer}>
        {isLoading ? (
          <CustomLoadingIndicator />
        ) : (
          <>
            {routines.length === 0 ? (
              <Text style={styles.noContentText}>
                Ainda não possui rotinas para vincular
              </Text>
            ) : (
              <>
                <Text style={styles.linkRoutineText}>
                  Selecionar rotina para vincular...
                </Text>
                <FlatList
                  data={routines}
                  contentContainerStyle={styles.cardList}
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({ item }) => (
                    <FlatListLinkRoutineItem
                      item={item}
                      navigation={navigation}
                      onClickItem={handleClickItem}
                    />
                  )}
                />
              </>
            )}
          </>
        )}
      </View>
      <ConfirmActionModal
        visible={visibleConfirmModal}
        modalTitle={modalConfirmMessage}
        onClose={() => setVisibleConfirmModal(false)}
        onConfirm={handleConfirmLink}
      />
    </>
  );
};

const FlatListLinkRoutineItem = ({ item, onClickItem }) => {
  const handlePress = (item) => {
    onClickItem(item);
  };

  return (
    <TouchableOpacity onPress={() => handlePress(item)}>
      <Text style={styles.textLinkedRoutineList}>{item.name}</Text>
    </TouchableOpacity>
  );
};

const PlanContent = ({ account, navigation }) => {
  const [form, setForm] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [defaultOptionSelectListPlan, setDefaultOptionSelectListPlan] =
    useState(null);

  useEffect(() => {
    setForm({
      expirationDate: account.expirationDate,
      plan: account.plan === "EXPIRED" ? "NORMAL_STUDENT" : account.plan,
    });
    setDefaultOptionSelectListPlan(handleSelectDefaultOptionPlan(account.plan));
    setIsLoading(false);
  }, []);

  const handleSelectDefaultOptionPlan = (plan) => {
    if (plan === "EXPIRED") return "NORMAL_STUDENT";
    return plan;
  };

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const handlePressDeactivateStudent = () => {
    setModalVisible(true);
  };

  const handleUpdatePlan = () => {
    updateStudentPlan(account.id, form)
      .then(() => {
        showSuccessToast("Plano atualizado com sucesso");
        navigation.navigate("AdminHome", { refreshListAllAccounts: true });
      })
      .catch((error) => handleError(error, "ao atualizar plano"));
  };

  const handleConfirmAction = () => {
    updateStudentPlan(account.id, {
      plan: "EXPIRED",
      expirationDate: convertDateToString(new Date()),
    })
      .then(() => showSuccessToast("Plano atualizado com sucesso"))
      .catch((error) => handleError(error, "ao atualizar plano"))
      .finally(() => {
        navigation.navigate("AdminHome", { refreshListAllAccounts: true });
        setModalVisible(false);
      });
  };

  return (
    <>
      {isLoading ? (
        <CustomLoadingIndicator />
      ) : (
        <>
          <DefaultDatePicker
            label="Escolha a data de expiração"
            onChange={(value) =>
              handleInputChange("expirationDate", convertDateToString(value))
            }
            value={form.expirationDate}
            initialValue={account.expirationDate}
          />
          <DefaultSelectList
            label="Escolha um plano"
            onChange={(value) => handleInputChange("plan", value)}
            data={defaultDataSelectListPlan}
            defaultOption={defaultOptionSelectListPlan}
          />
          <View style={styles.buttonsPlanContainer}>
            <DefaultButton
              animation="fadeInUp"
              textButton="Desativar"
              backgroundColor={theme.colors.error}
              onPress={handlePressDeactivateStudent}
            />
            <DefaultButton
              animation="fadeInUp"
              textButton="Alterar"
              onPress={handleUpdatePlan}
            />
          </View>
          <ConfirmActionModal
            visible={modalVisible}
            onClose={() => setModalVisible(false)}
            onConfirm={handleConfirmAction}
            modalTitle={`Deseja realmente desativar o plano do aluno ${account.displayName}?`}
            confirmButtonBackgroundColor={{
              backgroundColor: theme.colors.error,
            }}
            confirmButtonText="Confirmar"
          />
        </>
      )}
    </>
  );
};

const RoutineContent = ({ account, navigation, fetchData }) => {
  const [actionsModalVisible, setActionsModalVisible] = useState(false);
  const [actionItem, setActionItem] = useState(null);

  const handleClickCreateRoutine = () => {
    navigation.navigate("RoutineCreateEdit", { accountId: account.id });
  };

  const handleLongPressRoutineItem = (item) => {
    setActionsModalVisible(true);
    setActionItem(item);
  };

  return (
    <View style={styles.routineContainer}>
      <CreateCard onPress={handleClickCreateRoutine} />
      {account.routines.length === 0 ? (
        <View style={styles.noContentContainer}>
          <Text style={styles.noContentText}>
            Ainda não possui rotinas cadastradas
          </Text>
        </View>
      ) : (
        <FlatList
          data={account.routines.map((routine) =>
            convertRoutineToMinimalInfo(routine)
          )}
          contentContainerStyle={styles.cardList}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <FlatListRoutineItem
              item={item}
              navigation={navigation}
              onLongPress={handleLongPressRoutineItem}
            />
          )}
        />
      )}
      <ActionsRoutineModal
        account={account}
        actionItem={actionItem}
        visible={actionsModalVisible}
        navigation={navigation}
        onClose={() => setActionsModalVisible(false)}
        fetchData={fetchData}
      />
    </View>
  );
};

const FlatListRoutineItem = ({ item, navigation, onLongPress }) => {
  const handlePress = (item) => {
    navigation.navigate("RoutineCreateEdit", { id: item.id });
  };

  return (
    <TouchableOpacity
      onPress={() => handlePress(item)}
      onLongPress={() => onLongPress(item)}
    >
      <DefaultCard>
        <MinimalInfo data={item}>
          <FontAwesome name="angle-right" size={32} color="white" />
        </MinimalInfo>
      </DefaultCard>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingHorizontal: theme.spacing.medium,
    backgroundColor: theme.colors.background,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
  },
  noContentText: {
    color: "white",
    fontSize: theme.fontSizes.medium,
  },
  noContentContainer: {
    alignItems: "center",
  },
  contentContainer: {
    flex: 4,
    width: "100%",
  },
  routineContainer: {
    gap: theme.spacing.medium,
    marginBottom: theme.spacing.medium,
  },
  planContainer: {
    gap: theme.spacing.medium,
  },
  minimalInfoContainer: {
    width: "90%",
  },
  cardList: {
    gap: theme.spacing.medium,
  },
  textLinkedRoutineList: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
  },
  linkRoutineListContainer: {
    alignSelf: "center",
    width: "80%",
  },
  buttonsPlanContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    gap: theme.spacing.medium,
  },
  linkRoutineText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.large,
    fontWeight: theme.fontWeight.semiBold,
    marginBottom: theme.spacing.medium,
  },
});
