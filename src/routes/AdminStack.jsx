import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import AdminHome from "../screens/Admin/AdminHome";
import { ProfileEdit } from "../screens/Shared/ProfileEdit";
import Settings from "../screens/Shared/Settings";
import theme from "../theme";
import ExerciseLibrary from "../screens/Admin/ExerciseLibrary";
import RoutineLibrary from "../screens/Admin/RoutineLibrary";
import RoutineCreateEdit from "../screens/Admin/RoutineCreateEdit";
import ExerciseEdit from "../screens/Admin/ExerciseEdit";
import AdminStudentDetail from "../screens/Admin/AdminStudentDetail";
import WorkoutCreateEdit from "../screens/Admin/WorkoutCreateEdit";
import ExerciseDetailEdit from "../screens/Admin/ExerciseDetailEdit";
import { WorkoutProvider } from "../contexts/WorkoutContext";
import RoutineLink from "../screens/Admin/RoutineLink";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const options = { headerShown: false };

function AdminStack({ initialRouteName }) {
  return (
    <WorkoutProvider>
      <Stack.Navigator initialRouteName={initialRouteName}>
        <Stack.Screen
          name="AdminHome"
          component={AdminHome}
          options={options}
        />
        <Stack.Screen
          name="AdminSettings"
          component={Settings}
          options={options}
        />
        <Stack.Screen
          name="ProfileEdit"
          component={ProfileEdit}
          options={options}
        />
        <Stack.Screen
          name="ExerciseLibrary"
          component={ExerciseLibrary}
          options={options}
        />
        <Stack.Screen
          name="ExerciseEdit"
          component={ExerciseEdit}
          options={options}
        />
        <Stack.Screen
          name="AdminStudentDetail"
          component={AdminStudentDetail}
          options={options}
        />
        <Stack.Screen
          name="RoutineLibrary"
          component={RoutineLibrary}
          options={options}
        />
        <Stack.Screen
          name="RoutineCreateEdit"
          component={RoutineCreateEdit}
          options={options}
        />
        <Stack.Screen
          name="WorkoutCreateEdit"
          component={WorkoutCreateEdit}
          options={options}
        />
        <Stack.Screen
          name="ExerciseDetailEdit"
          component={ExerciseDetailEdit}
          options={options}
        />
        <Stack.Screen
          name="RoutineLink"
          component={RoutineLink}
          options={options}
        />
      </Stack.Navigator>
    </WorkoutProvider>
  );
}

export default function AdminTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          backgroundColor: theme.colors.background,
          borderTopColor: theme.colors.primary,
          borderTopWidth: 2,
        },
        tabBarActiveTintColor: theme.colors.primary,
        tabBarInactiveTintColor: theme.colors.lightText,
        tabBarLabelStyle: {
          fontSize: theme.fontSizes.small,
        },
        tabBarIconStyle: {
          marginTop: theme.spacing.small,
        },
      }}
    >
      <Tab.Screen
        name="HomeTab"
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="home" size={size} color={color} />
          ),
          tabBarLabel: "Início",
        }}
      >
        {() => <AdminStack initialRouteName="AdminHome" />}
      </Tab.Screen>
      <Tab.Screen
        name="SettingsTab"
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="settings" size={size} color={color} />
          ),
          tabBarLabel: "Configurações",
        }}
      >
        {() => <AdminStack initialRouteName="AdminSettings" />}
      </Tab.Screen>
    </Tab.Navigator>
  );
}
