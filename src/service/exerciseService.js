import api from "./api";
import { getToken } from "./tokenService";

export async function getAllMinimalExercises() {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/exercises`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export async function getExerciseById(exerciseId) {
  const bearerToken = await getToken();
  const response = await api.get(`/v1/admin/exercises/${exerciseId}`, {
    headers: { Authorization: bearerToken },
  });
  return response;
}

export async function getAllExercisesNotInWorkout(ignoredWorkoutId) {
  if (!ignoredWorkoutId) {
    return getAllMinimalExercises();
  }

  const bearerToken = await getToken();
  const response = await api.get(
    `/v1/admin/exercises?ignoredWorkoutId=${ignoredWorkoutId}`,
    {
      headers: { Authorization: bearerToken },
    }
  );
  return response;
}

export async function updateExerciseById(exercise) {
  const bearerToken = await getToken();
  const response = await api.put(
    `/v1/admin/exercises/${exercise.id}`,
    exercise,
    {
      headers: {
        Authorization: bearerToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response;
}

export async function deleteExercise(exerciseId) {
  const bearerToken = await getToken();
  const response = await api.delete(`/v1/admin/exercises/${exerciseId}`, {
    headers: { Authorization: bearerToken },
  });
  return response.data;
}

export async function createExercise(exercise) {
  const bearerToken = await getToken();
  const response = await api.post(`/v1/admin/exercises`, exercise, {
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
  });
  return response.data;
}
