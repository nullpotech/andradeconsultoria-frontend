import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import theme from "../theme";

export default function CustomLoadingIndicator({ transparency = true }) {
  const styles = StyleSheet.create({
    containerOverlay: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: transparency ? "rgba(0, 0, 0, 0.7)" : "rgba(0, 0, 0, 0)", // Fundo semi-transparente
      zIndex: 1000,
    },
  });

  return (
    <View style={styles.containerOverlay}>
      <ActivityIndicator size="large" color={theme.colors.primary} />
    </View>
  );
}
