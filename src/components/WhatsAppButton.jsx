import FontAwesome from "@expo/vector-icons/FontAwesome";
import { Linking, TouchableOpacity } from "react-native";
import React from "react";
import { showErrorToast } from "../utils/toastService";

export default function WhatsAppButton({
  phoneNumber,
  message = "Olá 👋, tudo bem? Aqui é o Administrador, passando para perguntar como andam as coisas aqui na plataforma?",
}) {
  const openWhatsApp = (phoneNumber, message) => {
    const url = `https://api.whatsapp.com/send?phone=${phoneNumber}&message=${message}`;
    Linking.canOpenURL(url)
      .then((supported) => {
        if (supported) {
          Linking.openURL(url);
        } else {
          showErrorToast("Não foi possível abrir o WhatsApp");
        }
      })
      .catch((err) => showErrorToast("Erro ao abrir o WhatsApp", err));
  };

  return (
    <TouchableOpacity onPress={() => openWhatsApp(phoneNumber, message)}>
      <FontAwesome name="whatsapp" size={36} color="green" />
    </TouchableOpacity>
  );
}
