const colors = {
  primary: "#D0FD3E",
  secondary: "#F3FFCE",
  background: "#000",
  lightBackground: "#303030",
  text: "#DDD",
  lightText: "#A7A3A3",
  blackText: "#111",
  cardBackground: "#2C2C2E",
  error: "#ff5252", 
  success: "#4caf50",
  warning: "#ffc107", 
};

const fontSizes = {
  xsmall: 10,
  small: 12,
  medium: 16,
  large: 20,
  xlarge: 24,
  xxlarge: 32,
  xxxLarge: 47,
};

const fontWeight = {
  light: "300",
  regular: "400",
  medium: "500",
  semiBold: "600",
  bold: "700",
  extraBold: "800",
};

const spacing = {
  xsmall: 4,
  small: 8,
  medium: 16,
  large: 24,
  xlarge: 32,
  xxlarge: 40,
};

const borderRadius = {
  small: 4,
  medium: 8,
  large: 16,
  xlarge: 24,
  full: 9999,
};

const borders = {
  thin: 1,
  medium: 2,
  thick: 4,
};

const opacity = {
  full: 1,
  high: 0.87,
  medium: 0.6,
  low: 0.38,
  disabled: 0.12,
};

const shadow = {
  small: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  
  medium: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  large: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
};

const theme = {
  colors,
  fontSizes,
  fontWeight,
  spacing,
  borderRadius,
  borders,
  opacity,
  shadow,
};

export default theme;
