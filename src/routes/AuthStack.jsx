import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import Auth from "../screens/Auth";
import PasswordReset from "../screens/Auth/PasswordReset";
import PasswordResetCode from "../screens/Auth/PasswordResetCode";

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Auth"
        component={Auth}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PasswordReset"
        component={PasswordReset}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PasswordResetCode"
        component={PasswordResetCode}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
