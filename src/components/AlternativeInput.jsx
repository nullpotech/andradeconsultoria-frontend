import React, { useState } from "react";
import { StyleSheet, TextInput, View } from "react-native";
import theme from "../theme";

export default function AlternativeInput({ onChange }) {
  const [text, setText] = useState("");

  const handleChangeText = (text) => {
    setText(text);
    onChange(text);
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Pesquisar por nome"
        placeholderTextColor={theme.colors.lightText}
        onChangeText={handleChangeText}
        value={text}
        style={styles.textInput}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: theme.colors.lightBackground,
    color: theme.colors.lightText,
    borderRadius: theme.borderRadius.large,
    padding: theme.spacing.medium,
  },
});
