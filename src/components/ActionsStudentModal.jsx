import { Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import theme from "../theme";

export const ActionsStudentModal = ({
  actionItem,
  visible,
  navigation,
  onClose,
  fetchData,
}) => {
  const handlePressCreate = () => {
    onClose();
    navigation.navigate("RoutineCreateEdit", { accountId: actionItem.id });
  };

  const handlePressImport = () => {
    onClose();
    navigation.navigate("AdminStudentDetail", {
      id: actionItem.id,
      paramActiveContent: "linked",
    });
  };

  const handlePressPlan = () => {
    onClose();
    navigation.navigate("AdminStudentDetail", {
      id: actionItem.id,
      paramActiveContent: "plan",
    });
  };

  return (
    <Modal
      transparent={true}
      visible={visible}
      animationType="fade"
      onRequestClose={onClose}
    >
      <View style={styles.modalOverlay}>
        <View style={styles.modalContainer}>
          <Text style={styles.modalTitle}>
            {actionItem ? actionItem.title : ""}
          </Text>
          <View style={styles.modalButtons}>
            <TouchableOpacity style={styles.button} onPress={handlePressCreate}>
              <Text style={styles.buttonText}>Criar Rotina</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={handlePressImport}>
              <Text style={styles.buttonText}>Importar Rotina</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={handlePressPlan}>
              <Text style={styles.buttonText}>Gerenciar Plano</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.buttonCancel]}
              onPress={onClose}
            >
              <Text style={styles.buttonTextCancel}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalOverlay: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContainer: {
    width: 300,
    height: 350,
    padding: theme.spacing.large,
    backgroundColor: theme.colors.background,
    borderRadius: theme.borderRadius.medium,
    borderColor: theme.colors.primary,
    borderWidth: theme.borders.thin,
    alignItems: "center",
    gap: theme.spacing.large,
  },
  modalTitle: {
    textAlign: "center",
    fontSize: theme.fontSizes.xlarge,
    fontWeight: theme.fontWeight.bold,
    marginBottom: theme.spacing.small,
    color: theme.colors.text,
  },
  modalMessage: {
    fontSize: theme.fontSizes.medium,
    color: theme.colors.text,
    marginBottom: theme.spacing.large,
    textAlign: "center",
  },
  modalButtons: {
    width: "100%",
    gap: theme.spacing.medium,
  },
  button: {
    paddingVertical: theme.spacing.small,
    marginRight: theme.spacing.small,
    backgroundColor: theme.colors.primary,
    borderRadius: theme.borderRadius.medium,
    alignItems: "center",
    borderWidth: theme.borders.thin,
    borderColor: theme.colors.primary,
  },
  buttonDanger: {
    backgroundColor: theme.colors.error,
    borderColor: theme.colors.danger,
  },
  buttonText: {
    color: theme.colors.blackText,
    fontSize: theme.fontSizes.large,
  },
  buttonCancel: {
    backgroundColor: theme.colors.background,
  },
  buttonTextCancel: {
    color: theme.colors.primary,
    fontSize: theme.fontSizes.large,
  },
});
