import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import theme from "../theme";
import DefaultCard from "./DefaultCard";

export default function CreateCard({ onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <DefaultCard>
        <View style={styles.contentCardContainer}>
          <MaterialIcons name="add" size={32} color={theme.colors.primary} />
        </View>
      </DefaultCard>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  contentCardContainer: {
    alignItems: "center",
    padding: theme.spacing.small,
  },
});
