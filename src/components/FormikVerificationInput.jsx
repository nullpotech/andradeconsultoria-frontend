import { StyleSheet, Text, TextInput } from "react-native";
import * as Animatable from "react-native-animatable";
import theme from "../theme";

export default function FormikLabelInput({
  value,
  onChangeText,
  onBlur,
  inputRef,
  onKeyPress,
  ...props
}) {
  return (
    <Animatable.View animation="fadeInDown">
      <TextInput
        style={styles.input}
        value={value}
        onChangeText={onChangeText}
        onBlur={onBlur}
        ref={inputRef}
        maxLength={1}
        keyboardType="numeric"
        onKeyPress={onKeyPress}
        {...props}
      />
    </Animatable.View>
  );
}

const styles = StyleSheet.create({
  input: {
    color: theme.colors.text,
    width: 45,
    height: 60,
    fontSize: theme.fontSizes.xlarge,
    textAlign: "center",
    backgroundColor: theme.colors.cardBackground,
  },
});
