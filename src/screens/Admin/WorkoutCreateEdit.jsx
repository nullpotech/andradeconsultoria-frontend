import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import ComponentTitle from "../../components/ComponentTitle";
import CustomLoadingIndicator from "../../components/CustomLoadingIndicator";
import ExerciseModal from "../../components/ExerciseModal";
import WorkoutForm from "../../components/WorkoutForm";
import { useWorkout } from "../../contexts/WorkoutContext";
import theme from "../../theme";

export default function WorkoutCreateEdit({ route, navigation }) {
  const { workoutId, routineId } = route.params;
  const {
    workout,
    setWorkout,
    isLoading,
    loadWorkout,
    saveWorkout,
    addExercises,
    removeExercise,
  } = useWorkout();
  const [modalVisible, setModalVisible] = useState(false);

  const handleInputChange = (field, value) => {
    setWorkout((prevWorkout) => ({
      ...prevWorkout,
      [field]: value,
    }));
  };

  const DEFAULT_DETAILS = {
    sets: 3,
    repetition: 15,
    load: 50,
    intervalSeconds: 90,
  };

  const handleConfirmAction = (exercises) => {
    const exercisesWithDetails = exercises.map((exercise) => {
      if (!exercise.details) {
        return { ...exercise, details: { ...DEFAULT_DETAILS } };
      }
      return exercise;
    });
    addExercises(exercisesWithDetails);
    setModalVisible(false);
  };

  const handlePressEdit = (exercise) => {
    navigation.navigate("ExerciseDetailEdit", {
      workoutId: workout.id,
      exercise,
    });
  };

  const handlePressSave = () => {
    saveWorkout();
    navigation.goBack();
  };

  useEffect(() => {
    loadWorkout(workoutId, routineId);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navigationContainer}>
        <ComponentTitle navigation={navigation}>
          <Text style={styles.componentTitleText}>
            {workout && workout.name ? workout.name : "Novo Treino"}
          </Text>
        </ComponentTitle>
      </View>
      {isLoading ? (
        <CustomLoadingIndicator transparency={false} />
      ) : (
        <View style={styles.contentContainer}>
          <WorkoutForm
            navigation={navigation}
            formData={workout}
            onChangeInput={handleInputChange}
            onPressAdd={setModalVisible}
            onPressEdit={handlePressEdit}
            onPressDelete={removeExercise}
            onPressSave={handlePressSave}
          />
          <ExerciseModal
            workoutId={workout.id}
            visible={modalVisible}
            onClose={() => setModalVisible(false)}
            onConfirm={handleConfirmAction}
            modalTitle="Grupo Muscular"
          />
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
    gap: theme.spacing.large,
  },
  navigationContainer: {
    flex: 1,
    alignItems: "center",
  },
  componentTitleText: {
    fontSize: theme.fontSizes.xlarge,
    textTransform: "uppercase",
    color: theme.colors.text,
  },
  contentContainer: {
    flex: 6,
    alignItems: "center",
    width: "100%",
  },
});
