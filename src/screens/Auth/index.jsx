import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  ImageBackground,
  Keyboard,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as Animatable from "react-native-animatable";
import * as Yup from "yup";
import DefaultButton from "../../components/DefaultButton";
import FormikLabelInput from "../../components/FormikLabelInput";
import { useAuth } from "../../contexts/AuthContext";
import theme from "../../theme";
import { showSuccessToast } from "../../utils/toastService";
import { handleError } from "../../service/errorService";

const { width } = Dimensions.get("window");

export default function Auth({ navigation }) {
  const { signIn, signUp } = useAuth();
  const [renderLoginScreen, setRenderLoginScreen] = useState(true);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  const [transparentValue, setTransparentValue] = useState(0.5);
  const [formStyleActiveKeyboard, setFormStyleActiveKeyboard] = useState({});

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      () => {
        setKeyboardVisible(true);
        setFormStyleActiveKeyboard({ flex: 3, justifyContent: "flex-start" });
        setTransparentValue(0.8);
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      () => {
        setKeyboardVisible(false);
        setFormStyleActiveKeyboard({});
        setTransparentValue(0.5);
      }
    );

    return () => {
      keyboardDidShowListener.remove();
      keyboardDidHideListener.remove();
    };
  }, []);

  const handleRegister = (registrationData) => {
    signUp(registrationData)
      .then(() => {
        showSuccessToast("Registro realizado com sucesso");
        setRenderLoginScreen(true);
      })
      .catch((error) => handleError(error, "ao registrar"));
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().required("Campo email é obrigatório"),
    password: Yup.string().required("Campo senha é obrigatório"),
    confirmPassword: Yup.string().when("password", {
      is: () => !renderLoginScreen,
      then: (schema) =>
        schema
          .oneOf([Yup.ref("password"), null], "As senhas devem ser iguais")
          .required("Campo confirmar senha é obrigatório")
          .min(8, "A senha deve ter pelo menos 8 caracteres")
          .test(
            "strong-password",
            "A senha deve conter pelo menos uma letra maiúscula, uma letra minúscula, um número e um caractere especial",
            (value) => {
              const strongPasswordRegex =
                /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
              return strongPasswordRegex.test(value || "");
            }
          ),
      otherwise: (schema) => schema.notRequired(),
    }),
  });

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../../assets/signin.jpg")}
        style={styles.imageContainer}
      >
        <View
          style={[
            styles.overlayImage,
            { backgroundColor: `rgba(0, 0, 0, ${transparentValue})` },
          ]}
        />

        <View style={styles.menuContainer}>
          <TouchableOpacity
            style={[
              styles.menuButton,
              renderLoginScreen && styles.activeMenuButton,
            ]}
            onPress={() => {
              setRenderLoginScreen(true);
            }}
          >
            <Text style={styles.menuButtonText}>Entrar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.menuButton,
              !renderLoginScreen && styles.activeMenuButton,
            ]}
            onPress={() => {
              setRenderLoginScreen(false);
            }}
          >
            <Text style={styles.menuButtonText}>Registrar</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.welcomeContainer}>
          {!isKeyboardVisible && (
            <View style={styles.welcomeContainer}>
              {renderLoginScreen ? (
                <Animatable.Text
                  animation="fadeInLeft"
                  style={styles.welcomeTitle}
                >
                  Bem Vindo!
                </Animatable.Text>
              ) : (
                <Animatable.View animation="fadeInLeft">
                  <Text style={styles.welcomeTitle}>Olá Novato,</Text>
                  <Text style={styles.welcomeText}>
                    Que bom ter você aqui, preencha
                  </Text>
                  <Text style={styles.welcomeText}>os seus dados abaixo.</Text>
                </Animatable.View>
              )}
            </View>
          )}
        </View>

        <View style={styles.triangle} />
      </ImageBackground>

      <View style={[styles.formContainer, formStyleActiveKeyboard]}>
        <Formik
          initialValues={{ email: "", password: "", confirmPassword: "" }}
          validationSchema={validationSchema}
          onSubmit={(values, { setFieldValue, setFieldTouched }) => {
            if (renderLoginScreen) {
              signIn(values);
            } else {
              handleRegister(values);
              setFieldValue("password", "");
              setFieldTouched("password", false);
              setFieldValue("confirmPassword", "");
              setFieldTouched("confirmPassword", false);
            }
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              <FormikLabelInput
                label="Email"
                value={values.email}
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                touched={touched.email}
                errors={errors.email}
                keyboardType="email-address"
              />
              <FormikLabelInput
                label="Senha"
                value={values.password}
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                touched={touched.password}
                errors={errors.password}
                secureTextEntry={true}
              />

              {renderLoginScreen ? (
                <>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("PasswordReset")}
                  >
                    <Animatable.Text
                      animation="fadeInUp"
                      style={styles.textLink}
                    >
                      Esqueceu a senha?
                    </Animatable.Text>
                  </TouchableOpacity>
                  <DefaultButton
                    textButton="Entrar"
                    iconName="caretright"
                    animation="fadeInUp"
                    onPress={handleSubmit}
                  />
                </>
              ) : (
                <>
                  <FormikLabelInput
                    label="Confirmar Senha"
                    value={values.confirmPassword}
                    onChangeText={handleChange("confirmPassword")}
                    onBlur={handleBlur("confirmPassword")}
                    touched={touched.confirmPassword}
                    errors={errors.confirmPassword}
                    secureTextEntry={true}
                  />
                  <DefaultButton
                    textButton="Registrar"
                    iconName="caretright"
                    animation="fadeInUp"
                    onPress={handleSubmit}
                  />
                </>
              )}
            </>
          )}
        </Formik>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  imageContainer: {
    flex: 1,
    justifyContent: "space-between",
    gap: theme.spacing.large,
    width: width,
  },
  overlayImage: {
    ...StyleSheet.absoluteFillObject,
  },
  menuContainer: {
    flexDirection: "row",
    gap: theme.spacing.medium,
    position: "absolute",
    top: 60,
    left: 45,
  },
  menuButton: {
    padding: theme.spacing.small,
    borderRadius: 10,
  },
  activeMenuButton: {
    borderBottomColor: theme.colors.primary,
    borderBottomWidth: 2,
  },
  menuButtonText: {
    color: theme.colors.text,
    textTransform: "uppercase",
    fontSize: theme.fontSizes.medium,
    fontWeight: theme.fontWeight.semiBold,
    textAlign: "center",
  },
  welcomeContainer: {
    position: "absolute",
    zIndex: 1,
    left: 20,
    bottom: 35,
  },
  welcomeTitle: {
    fontWeight: theme.fontWeight.bold,
    color: theme.colors.text,
    fontSize: theme.fontSizes.xxxLarge,
  },
  welcomeText: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.medium,
  },
  triangle: {
    borderRightWidth: width,
    borderTopWidth: 60,
    position: "absolute",
    bottom: 0,
    borderStyle: "solid",
    borderRightColor: "black",
    borderTopColor: "transparent",
  },
  formContainer: {
    flex: 2,
    width: width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: theme.colors.background,
    gap: theme.spacing.small,
  },
  textLink: {
    color: theme.colors.text,
    fontSize: theme.fontSizes.medium,
    textAlign: "center",
  },
});
