import FontAwesome5 from "@expo/vector-icons/FontAwesome5";
import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import React, { useState } from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import * as Animatable from "react-native-animatable";
import { SafeAreaView } from "react-native-safe-area-context";
import Icon from "react-native-vector-icons/FontAwesome";
import { ConfirmActionModal } from "../../components/ConfirmActionModal";
import Logo from "../../components/Logo";
import ProfileGreeting from "../../components/ProfileGreeting";
import { useAuth } from "../../contexts/AuthContext";
import theme from "../../theme";

export default function StudentHome({ navigation }) {
  const { user, logout } = useAuth();
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <Logo />
      <View style={styles.greetingContainer}>
        <ProfileGreeting
          userName={user?.name}
          userProfileImageUrl={require("../../../assets/photoPerfil1.png")}
          navigation={navigation}
        />
        <TouchableOpacity onPress={logout}>
          <Text style={styles.logoutText}>Sair</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuContainer}>
        {user?.plan === "EXPIRED" ? (
          <View style={{ gap: theme.spacing.large }}>
            <Animatable.Text
              animation="fadeInLeft"
              style={[styles.expiredText, { color: theme.colors.error }]}
            >
              PLANO EXPIRADO!
            </Animatable.Text>
            <Animatable.Text animation="fadeInRight" style={styles.expiredText}>
              Seu plano ainda não iniciou ou está expirado, entre em contato com o administrador.
            </Animatable.Text>
          </View>
        ) : (
          <>
            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => navigation.navigate("StudentRoutine")}
            >
              <FontAwesome5
                name="dumbbell"
                size={30}
                color={theme.colors.textOnPrimary}
              />
              <Text style={styles.menuItemText}>Treinos</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => navigation.navigate("AnamneseForm")}
            >
              <FontAwesome5
                name="notes-medical"
                size={30}
                color={theme.colors.textOnPrimary}
              />
              <Text style={styles.menuItemText}>Anamnese</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => setModalVisible(true)}
            >
              <Icon name="apple" size={30} color={theme.colors.textOnPrimary} />
              <Text style={styles.menuItemText}>Alimentação</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => setModalVisible(true)}
            >
              <Icon name="book" size={30} color={theme.colors.textOnPrimary} />
              <Text style={styles.menuItemText}>Aulas</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuItem}
              onPress={() => setModalVisible(true)}
            >
              <MaterialIcons
                name="feedback"
                size={30}
                color={theme.colors.textOnPrimary}
              />
              <Text style={styles.menuItemText}>Feedback</Text>
            </TouchableOpacity>
          </>
        )}
      </View>
      <ConfirmActionModal
        visible={modalVisible}
        onConfirm={() => setModalVisible(false)}
        modalTitle="Esta funcionalidade estará disponível em breve."
        confirmButtonText="Entendido"
        showCancelButton={false}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    paddingHorizontal: theme.spacing.medium,
  },
  greetingContainer: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    marginBottom: theme.spacing.large,
    marginTop: theme.spacing.large,
  },
  logoutText: {
    paddingRight: theme.spacing.medium,
    color: theme.colors.text,
  },
  menuContainer: {
    flex: 1,
    marginTop: theme.spacing.large,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  menuItem: {
    width: "45%",
    margin: theme.spacing.small,
    padding: theme.spacing.medium,
    alignItems: "center",
    backgroundColor: theme.colors.primary,
    borderRadius: theme.spacing.small,
  },
  menuItemText: {
    color: theme.colors.textOnPrimary,
    fontSize: theme.fontSizes.medium,
    marginTop: theme.spacing.small,
  },
  expiredText: {
    fontSize: theme.fontSizes.xlarge,
    color: theme.colors.text,
    textAlign: "center",
    padding: theme.spacing.medium,
  },
});
