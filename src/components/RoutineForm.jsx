import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { handleError } from "../service/errorService";
import theme from "../theme";
import {
  defaultDataSelectListDifficulty,
  defaultDataSelectListGoal,
} from "../utils/constDefaultDataSelectList";
import { showSuccessToast } from "../utils/toastService";
import DefaultButton from "./DefaultButton";
import { DefaultDatePicker } from "./DefaultDatePicker";
import DefaultLabelInput from "./DefaultLabelInput";
import DefaultSelectList from "./DefaultSelectList";
import { createRoutine, updateRoutineById } from "../service/routineService";
import { convertDateToString } from "../utils/converter";

export default function RoutineForm({ initialValues, onSubmit }) {
  const [isLoading, setIsLoading] = useState(true);
  const [form, setForm] = useState(null);

  const fetchDefaultData = () => {
    const nowDate = new Date();
    let oneMonthLater = new Date(nowDate);
    oneMonthLater.setMonth(nowDate.getMonth() + 1);
    setForm({
      id: initialValues.id,
      name: initialValues.name,
      goal: initialValues.goal,
      difficulty: initialValues.difficulty,
      initDate: initialValues.initDate ? initialValues.initDate : nowDate,
      endDate: initialValues.endDate ? initialValues.endDate : oneMonthLater,
      accountId: initialValues.accountId ? initialValues.accountId : null,
    });
    setIsLoading(false);
  };

  useEffect(() => {
    fetchDefaultData();
  }, []);

  const handleInputChange = (field, value) => {
    setForm((prevForm) => ({
      ...prevForm,
      [field]: value,
    }));
  };

  const handleSubmit = (form) => {
    form.initDate = convertDateToString(form.initDate);
    form.endDate = convertDateToString(form.endDate);

    if (form.id === null) {
      createRoutine(form)
        .then((response) => {
          showSuccessToast("Rotina salva com sucesso");
          onSubmit(response.data);
        })
        .catch((error) => handleError(error, "ao salvar rotina"));
    } else {
      updateRoutineById(form)
        .then(() => showSuccessToast("Rotina atualizada com sucesso"))
        .catch((error) => handleError(error, "ao atualizar rotina"));
    }
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      {!isLoading && (
        <>
          <DefaultLabelInput
            label="Nome"
            value={form.name}
            onChange={(value) => handleInputChange("name", value)}
          />
          <DefaultSelectList
            data={defaultDataSelectListGoal}
            defaultOption={form.goal}
            label="Objetivo"
            onChange={(value) => handleInputChange("goal", value)}
          />
          <DefaultSelectList
            data={defaultDataSelectListDifficulty}
            defaultOption={form.difficulty}
            label="Dificuldade"
            onChange={(value) => handleInputChange("difficulty", value)}
          />
          <DefaultDatePicker
            label="Data Início"
            initialValue={form.initDate}
            onChange={(value) => handleInputChange("initDate", value)}
          />
          <DefaultDatePicker
            label="Data Fim"
            initialValue={form.endDate}
            onChange={(value) => handleInputChange("endDate", value)}
          />
          <View style={styles.buttonContainer}>
            <DefaultButton
              animation="fadeInUp"
              textButton="salvar"
              onPress={() => handleSubmit(form)}
            />
          </View>
        </>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: theme.spacing.xlarge,
  },
});
