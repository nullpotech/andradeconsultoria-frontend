import FontAwesome from "@expo/vector-icons/FontAwesome";
import MaterialIcons from "@expo/vector-icons/MaterialIcons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Linking } from "react-native";
import { useAuth } from "../contexts/AuthContext";
import { AnamneseForm } from "../screens/Shared/AnamneseForm";
import { ProfileEdit } from "../screens/Shared/ProfileEdit";
import StudentHome from "../screens/Student/StudentHome";
import StudentRoutine from "../screens/Student/StudentRoutine";
import Welcome from "../screens/Student/Welcome";
import theme from "../theme";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const options = { headerShown: false };

function StudentStack() {
  const { user } = useAuth();

  return (
    <Stack.Navigator
      initialRouteName={user && user.name ? "StudentHome" : "Welcome"}
    >
      <Stack.Screen
        name="StudentHome"
        component={StudentHome}
        options={options}
      />
      <Stack.Screen
        name="ProfileEdit"
        component={ProfileEdit}
        options={options}
      />
      <Stack.Screen
        name="StudentRoutine"
        component={StudentRoutine}
        options={options}
      />
      <Stack.Screen
        name="AnamneseForm"
        component={AnamneseForm}
        options={options}
      />
      <Stack.Screen name="Welcome" component={Welcome} options={options} />
    </Stack.Navigator>
  );
}

export default function StudentTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          backgroundColor: theme.colors.background,
          borderTopColor: theme.colors.primary,
          borderTopWidth: 2,
        },
        tabBarActiveTintColor: theme.colors.primary,
        tabBarInactiveTintColor: theme.colors.lightText,
        tabBarLabelStyle: {
          fontSize: theme.fontSizes.small,
        },
        tabBarIconStyle: {
          marginTop: theme.spacing.small,
        },
      }}
    >
      <Tab.Screen
        name="HomeTab"
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="home" size={size} color={color} />
          ),
          tabBarLabel: "Início",
        }}
      >
        {() => <StudentStack initialRouteName="StudentHome" />}
      </Tab.Screen>
      <Tab.Screen
        name="Instagram"
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="instagram" size={size - 1} color={color} />
          ),
          tabBarLabel: "Instagram",
        }}
        listeners={{
          tabPress: (e) => {
            e.preventDefault();
            const instagramUrl = "https://instagram.com/_treinadorandrade";
            const appUrl = "instagram://user?username=_treinadorandrade";
            Linking.canOpenURL(appUrl)
              .then((supported) => {
                if (supported) {
                  Linking.openURL(appUrl);
                } else {
                  Linking.openURL(instagramUrl);
                }
              })
              .catch((err) => console.error("An error occurred", err));
          },
        }}
      >
        {() => null}
      </Tab.Screen>

      <Tab.Screen
        name="Whatsapp"
        options={{
          headerShown: false,
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="whatsapp" size={size - 1} color={color} />
          ),
          tabBarLabel: "Whatsapp",
        }}
        listeners={{
          tabPress: (e) => {
            e.preventDefault();
            Linking.openURL("https://api.whatsapp.com/send?phone=555194883333");
          },
        }}
      >
        {() => null}
      </Tab.Screen>
    </Tab.Navigator>
  );
}
